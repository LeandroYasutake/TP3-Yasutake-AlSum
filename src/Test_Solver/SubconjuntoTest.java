package Test_Solver;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import Logica_Materias.ListadoMaterias;
import Logica_Solver.Subconjunto;


public class SubconjuntoTest 
{
	@Test
	public void testCrearInstancia() 
	{
		Subconjunto subconjunto = new Subconjunto();
		
		assertEquals(0, subconjunto.getAulas().size(), 0);
	}
	
	@Test
	public void testResetearAulas() 
	{
		Subconjunto subconjunto = new Subconjunto();
		ListadoMaterias aula = new ListadoMaterias();
		ListadoMaterias aula1 = new ListadoMaterias();
		ListadoMaterias aula2 = new ListadoMaterias();
		ArrayList<ListadoMaterias> aulas = new ArrayList<ListadoMaterias>();
		
		aulas.add(aula);
		aulas.add(aula1);
		aulas.add(aula2);
		
		subconjunto.setAulas(aulas);
		
		assertEquals(3, subconjunto.getAulas().size(), 0);
		
		subconjunto.resetearAulas();
		
		assertEquals(0, subconjunto.getAulas().size(), 0);
	}
	
	@Test
	public void testAulasVacias() 
	{
		Subconjunto subconjunto = new Subconjunto();
		ListadoMaterias aula = new ListadoMaterias();
		ListadoMaterias aula1 = new ListadoMaterias();
		ListadoMaterias aula2 = new ListadoMaterias();
		ArrayList<ListadoMaterias> aulas = new ArrayList<ListadoMaterias>();
		
		aulas.add(aula);
		aulas.add(aula1);
		aulas.add(aula2);
		
		subconjunto.setAulas(aulas);
		
		assertEquals(3, subconjunto.getAulas().size(), 0);
		
		subconjunto.eliminarAulasVacias();
		
		assertEquals(0, subconjunto.getAulas().size(), 0);
	}
}
