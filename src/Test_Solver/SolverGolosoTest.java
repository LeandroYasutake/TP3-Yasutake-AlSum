package Test_Solver;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Instancia;
import Logica_Solver.SolverGoloso;

public class SolverGolosoTest 
{
	private Instancia inst;

	@Before
	public void inicializar()
	{
		inst = new Instancia();
		ListadoMaterias materias = new ListadoMaterias();
		
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 9 , 10));
		inst.setMaterias(materias);
	}

	@Test (expected=NullPointerException.class)
	public void testCrearSolverGoloso() 
	{
		inst = null;
		@SuppressWarnings("unused")
		SolverGoloso solver = new SolverGoloso(inst);
	}
	
	@Test 
	public void testResolver() 
	{
		SolverGoloso solver = new SolverGoloso(inst);
		
		solver.resolverSimple();
		
		assertEquals(2, solver.getAulasAsignadas().size(), 0);
	}
	
	@Test 
	public void testResolverUnAula() 
	{
		inst.getMaterias().remove(new Materia("b", 8 , 9));
		SolverGoloso solver = new SolverGoloso(inst);
		
		solver.resolverSimple();
		
		assertEquals(1, solver.getAulasAsignadas().size(), 0);
	}
	
	@Test (expected=IndexOutOfBoundsException.class)
	public void testResolverVacio() 
	{
		inst = new Instancia();
		SolverGoloso solver = new SolverGoloso(inst);
		
		solver.resolverSimple();
	}
}
