package Test_Solver;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Instancia;
import Logica_Solver.SolverGenetico;

public class SolverGeneticoTest 
{
	private Instancia inst;

	@Before
	public void inicializar()
	{
		inst = new Instancia();
		ListadoMaterias materias = new ListadoMaterias();
		
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 9 , 10));
		inst.setMaterias(materias);
	}
	
	@Test
	public void testCrearSolverGenetico() 
	{
		@SuppressWarnings("unused")
		SolverGenetico solver = new SolverGenetico(inst, 0);
	}
	
	@Test 
	public void testResolver() 
	{
		SolverGenetico solver = new SolverGenetico(inst, 2);
		
		solver.resolverGenetico();
		
		assertEquals(2, solver.getAulasAsignadas().size(), 0);
	}
	
	@Test 
	public void testResolverUnAula() 
	{
		inst.getMaterias().remove(new Materia("b", 8 , 9));
		SolverGenetico solver = new SolverGenetico(inst, 1);
		
		solver.resolverGenetico();
		
		assertEquals(1, solver.getAulasAsignadas().size(), 0);
	}
	
	@Test
	public void testResolverVacio() 
	{
		inst = new Instancia();
		SolverGenetico solver = new SolverGenetico(inst, 0);
		
		solver.resolverGenetico();
		
		assertEquals(0, solver.getAulasAsignadas().size(), 0);
	}
}
