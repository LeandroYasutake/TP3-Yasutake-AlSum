package Test_Solver;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Instancia;
import Logica_Solver.SolverColoreoGoloso;

public class SolverColoreoGolosoTest
{
	private Instancia inst;

	@Before
	public void inicializar()
	{
		inst = new Instancia();
		ListadoMaterias materias = new ListadoMaterias();
		
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 9 , 10));
		inst.setMaterias(materias);
	}
	
	@Test
	public void testCrearSolverColoreoGoloso() 
	{
		@SuppressWarnings("unused")
		SolverColoreoGoloso solver = new SolverColoreoGoloso(inst);
	}
	
	@Test 
	public void testResolver() 
	{
		SolverColoreoGoloso solver = new SolverColoreoGoloso(inst);
		
		solver.resolverConColor();
		
		assertEquals(2, solver.getAulasAsignadas().size(), 0);
	}
	
	@Test 
	public void testResolverUnAula() 
	{
		inst.getMaterias().remove(new Materia("b", 8 , 9));
		SolverColoreoGoloso solver = new SolverColoreoGoloso(inst);
		
		solver.resolverConColor();
		
		assertEquals(1, solver.getAulasAsignadas().size(), 0);
	}
	
	@Test 
	public void testResolverVacio() 
	{
		inst = new Instancia();
		SolverColoreoGoloso solver = new SolverColoreoGoloso(inst);
		
		solver.resolverConColor();
		
		assertEquals(0, solver.getAulasAsignadas().size(), 0);
	}
}
