package Logica_Materias;

import java.util.ArrayList;
import java.util.Objects;

import Logica_Grafo.Grafo;


public class ListadoMaterias 
{
	private ArrayList<Materia> materiasSinAulas;
	private int codigo;
	
	public ListadoMaterias()
	{
		this.materiasSinAulas = new ArrayList<Materia>();
		this.codigo = 0;
	}
	
	public ArrayList<Materia> getMaterias()
	{
		return this.materiasSinAulas;
	}
	
	public int getCodigo()
	{
		return this.codigo;
	}
	
	public int getNumeroMaterias()
	{
		return this.materiasSinAulas.size();
	}
	
	public Materia getMateria(int materia)
	{
		return this.materiasSinAulas.get(materia);
	}
	
	public void addMateria(Materia materia)
	{
		materia.setCodigo(this.codigo);
		this.codigo++;
		this.materiasSinAulas.add(materia);
	}
	
	public void removeMateria(Materia materia)
	{
		if (!this.materiasSinAulas.contains(materia))
			throw new IllegalArgumentException("La materia no existe en el listado!");
		
		this.materiasSinAulas.remove(materia);
	}
	
	public Grafo crearGrafo()
	{
		Grafo grafo = new Grafo(materiasSinAulas.size());
		int posicion1;
		int posicion2;
		
		for (Materia materia : materiasSinAulas) 
			for (Materia otraMateria : materiasSinAulas)
				if (!materia.equals(otraMateria) && materia.seSuperpone(otraMateria))
				{
					posicion1 = getPosicion(materia);
					posicion2 = getPosicion(otraMateria);
					grafo.agregarArista(posicion1, posicion2);
				}
		
		return grafo;
	}

	public int getPosicion(Materia materia)
	{
		for (int pos = 0 ; pos < materiasSinAulas.size() ; pos++)
			if (materiasSinAulas.get(pos).equals(materia))
				return pos;
		
		throw new IllegalArgumentException("La materia no se encuentra en el listado");
	}
	
	public boolean hayOcupacion(int horaInicio, int horaFin) 
	{
		for (Materia materia : materiasSinAulas)
		{
			if (materia.getHoraInicio() == horaInicio)
				return true;
			
			else if (materia.getHoraFin() == horaFin)
				return true;
			
			else if (materia.getHoraInicio() < horaFin &&
					horaInicio < materia.getHoraFin())
				return true;
		}
		
		return false;
	}
	
	public ArrayList<Materia> clonarMaterias()
	{
		ArrayList<Materia> ret = new ArrayList<Materia>();
		
		for (Materia materia : materiasSinAulas)
		{
			Materia nuevaMat = new Materia(materia.getNombre(), materia.getHoraInicio(), materia.getHoraFin());
			nuevaMat.setCodigo(materia.getCodigo());
			ret.add(nuevaMat);
		}
	
		return ret;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		ListadoMaterias other = (ListadoMaterias) obj;
		
		return (Objects.equals(this.materiasSinAulas, other.getMaterias()));
	}

	@Override
	public String toString() 
	{
		String ret = "[";
		for(Materia m : this.materiasSinAulas)
			ret += " "+m.getNombre();
		ret += " ]";
		return ret;
	}
}
