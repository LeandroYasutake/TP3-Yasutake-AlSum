package Logica_Materias;

import java.util.Objects;

public class Materia 
{
	private String nombre;
	private int codigo;
	private int horaInicio;
	private int horaFin;
	
	public Materia (String nombre, int inicio, int fin)
	{
		this.nombre = nombre;
		this.horaInicio = inicio;
		this.horaFin = fin;
	}
	
	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public int getCodigo() 
	{
		return codigo;
	}

	public void setCodigo(int codigo) 
	{
		this.codigo = codigo;
	}

	public int getHoraInicio() 
	{
		return horaInicio;
	}

	public void setHoraInicio(int nuevahoraInicio) 
	{
		this.horaInicio = nuevahoraInicio;
	}
	
	public int getHoraFin() 
	{
		return horaFin;
	}

	public void setHoraFin(int nuevahoraFin) 
	{
		this.horaFin = nuevahoraFin;
	}
	
	public boolean seSuperpone(Materia otraMateria) 
	{
		if (getHoraInicio() == otraMateria.getHoraInicio() ||
			getHoraFin() == otraMateria.getHoraFin())
			return true;
		
		else if (getHoraInicio() < otraMateria.getHoraFin() &&
				otraMateria.getHoraInicio() < getHoraFin())
			return true;
		
		else
			return false;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (obj == null) 
			return false;
		if (obj == this) 
			return true;
		if (!(obj instanceof Materia))
			return false;
		
		Materia other = (Materia) obj;
		
		return (Objects.equals(this.nombre, other.getNombre()) &&
				this.horaInicio == other.getHoraInicio() &&
				this.horaFin == other.getHoraFin());
	}

	@Override
	public String toString() 
	{
		return this.nombre;
	}	
}
