package Logica_Materias;

import java.util.ArrayList;

public class GestorAulas 
{
	public static double[] getAulasSubutilizadasYpromedio(ArrayList<ListadoMaterias> aulasAsignadas)
	{
		int aulasSub = 0;
		int horasAula = 0;
		int CantMaterias = 0;
		double horasTotales = 0;
		
		for (ListadoMaterias aula : aulasAsignadas)
		{
			horasAula = 0;
			
			for (Materia m : aula.getMaterias())
			{
				CantMaterias++;
				
				int horarioInicial = m.getHoraInicio();
				
				while(horarioInicial != m.getHoraFin())
				{
					horasAula++;
					horasTotales++;
					horarioInicial++;
				}
			}
			
			if (horasAula <= 5)
				aulasSub++;
		}
		
		return new double[] {aulasSub, horasTotales/(double)aulasAsignadas.size(), CantMaterias};
	}
	
	public static double getPromedioMateriasAulas(ArrayList<ListadoMaterias> aulasAsignadas)
	{
		double cantidadMaterias = 0;
		
		for (ListadoMaterias aula : aulasAsignadas)
			cantidadMaterias += aula.getMaterias().size();
					
		return cantidadMaterias/ (double)aulasAsignadas.size();
	}
	
	public static double[] getInfoAula(ListadoMaterias aula)
	{
		double horasUtilizadas = 0;
		
		for (Materia materia : aula.getMaterias())
		{
			int horarioInicial = materia.getHoraInicio();
			
			while(horarioInicial != materia.getHoraFin())
			{
				horasUtilizadas++;
				horarioInicial++;
			}
		}
		
		return new double[] {horasUtilizadas, 14-horasUtilizadas};
	}
	
	public static boolean intercambiarMaterias(Materia materiaSeleccionada,
			ListadoMaterias aulaAuxOrigen, ListadoMaterias aulaAuxDestino)
	{
		if (aulaAuxDestino.hayOcupacion(materiaSeleccionada.getHoraInicio(), materiaSeleccionada.getHoraFin()))
			return false;
		
		aulaAuxOrigen.removeMateria(materiaSeleccionada);
		aulaAuxDestino.addMateria(materiaSeleccionada);
		
		return true;
	}
}


