package Test_Materias;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import Logica_Grafo.Grafo;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;

public class ListadoMateriasTest 
{
	@Test
	public void testCrearListado() 
	{
		ListadoMaterias listado = new ListadoMaterias ();
		
		assertEquals(0, listado.getNumeroMaterias());
		assertEquals(0, listado.getCodigo());
	}
	
	@Test
	public void testAgregarMateria() 
	{
		ListadoMaterias listado = new ListadoMaterias ();
		Materia materia = new Materia ("x", 8 , 9);
		Materia materia2 = new Materia ("x", 8 , 9);
		
		listado.addMateria(materia);
		
		assertEquals(1, listado.getNumeroMaterias());
		assertEquals(1, listado.getCodigo());
		assertEquals(0, materia.getCodigo());
		
		listado.addMateria(materia2);
		
		assertEquals(2, listado.getNumeroMaterias());
		assertEquals(2, listado.getCodigo());
		assertEquals(1, materia2.getCodigo());
	}
	
	@Test
	public void testRemoverMateria() 
	{
		ListadoMaterias listado = new ListadoMaterias ();
		Materia materia = new Materia ("x", 8 , 9);
		
		listado.addMateria(materia);
		listado.removeMateria(materia);
		
		assertEquals(0, listado.getNumeroMaterias());
		assertEquals(1, listado.getCodigo());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRemoverMateriaInexistente() 
	{
		ListadoMaterias listado = new ListadoMaterias ();
		
		listado.removeMateria(new Materia ("x", 8 , 9));
	}
	
	@Test
	public void testCrearGrafo() 
	{
		ListadoMaterias listado = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 8 , 9);
		Materia materia3 = new Materia ("c", 9 , 10);
		Grafo grafoListado;
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		grafoListado = listado.crearGrafo();
		
		assertEquals(3, grafoListado.cantidadDeVertices());
		assertEquals(1, grafoListado.cantidadDeAristas());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetPosicion() 
	{
		ListadoMaterias listado = new ListadoMaterias ();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 8 , 9);
		Materia materia3 = new Materia ("c", 9 , 10);
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		
		assertEquals(0, listado.getPosicion(materia));
		assertEquals(1, listado.getPosicion(materia2));
		assertEquals(2, listado.getPosicion(materia3));
		
		assertEquals(0, listado.getPosicion(new Materia("inexistente", 8, 9)));
	}
	
	@Test
	public void testHayOcupacion() 
	{
		ListadoMaterias listado = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 8 , 9);
		Materia materia3 = new Materia ("c", 9 , 10);
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		
		assertTrue(listado.hayOcupacion(8, 11));
		assertFalse(listado.hayOcupacion(11, 11));
	}
	
	@Test
	public void testClonarMaterias() 
	{
		ListadoMaterias listado = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 8 , 9);
		Materia materia3 = new Materia ("c", 9 , 10);
		ArrayList<Materia> clon;
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		
		clon = listado.clonarMaterias();
		
		assertEquals(true, clon.equals(listado.getMaterias()));
	}
	
	@Test
	public void testEquals() 
	{
		ListadoMaterias listado = new ListadoMaterias();
		ListadoMaterias listado2 = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 8 , 9);
		Materia materia3 = new Materia ("c", 9 , 10);
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		
		listado2.addMateria(materia);
		listado2.addMateria(materia2);
		listado2.addMateria(materia3);
		
		assertEquals(true, listado.equals(listado2));
		
		listado2.removeMateria(materia3);
		
		assertEquals(false, listado.equals(listado2));
	}
}
