package Test_Materias;

import static org.junit.Assert.*;

import org.junit.Test;

import Logica_Materias.Materia;

public class MateriaTest 
{
	@Test
	public void testCrearMateria() 
	{
		Materia materia = new Materia ("x", 8, 9);
		
		assertEquals("x", materia.getNombre());
		assertEquals(8, materia.getHoraInicio());
		assertEquals(9, materia.getHoraFin());
	}
	
	@Test
	public void testHayConflicto() 
	{
		Materia materia = new Materia ("x", 8, 9);
		Materia materia2 = new Materia ("x", 8, 9);
		
		assertTrue(materia.seSuperpone(materia2));
	}
	
	@Test
	public void testHayConflictoBorde1() 
	{
		Materia materia = new Materia ("x", 8, 9);
		Materia materia2 = new Materia ("x", 9, 10);
		
		assertFalse(materia.seSuperpone(materia2));
	}
	
	@Test
	public void testHayConflictoBorde2() 
	{
		Materia materia = new Materia ("x", 8 ,9);
		Materia materia2 = new Materia ("x", 7, 8);
		
		assertFalse(materia.seSuperpone(materia2));
	}
	
	@Test
	public void testEquals() 
	{
		Materia materia0 = new Materia ("x", 8, 9);
		Materia materia1 = new Materia ("x", 8, 9);
		Materia materia2 = new Materia ("c", 8, 9);
		Materia materia3 = new Materia ("x", 10, 11);
		
		assertEquals(true, materia0.equals(materia1));
		assertEquals(false, materia0.equals(materia2));
		assertEquals(false, materia0.equals(materia3));
	}
}
