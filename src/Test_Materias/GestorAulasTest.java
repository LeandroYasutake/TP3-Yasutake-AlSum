package Test_Materias;

import static org.junit.Assert.*;

import org.junit.Test;

import Logica_Materias.GestorAulas;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Subconjunto;

public class GestorAulasTest 
{
	@Test
	public void testGetAulasSubutilizadasYpromedio() 
	{
		Subconjunto solucion = new Subconjunto();
		ListadoMaterias listado = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 12 , 13);
		Materia materia3 = new Materia ("c", 9 , 10);
		double aulasSub, horasAula, cantMat ;
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		solucion.getAulas().add(listado);
		
		aulasSub = GestorAulas.getAulasSubutilizadasYpromedio(solucion.getAulas())[0];
		horasAula = GestorAulas.getAulasSubutilizadasYpromedio(solucion.getAulas())[1];
		cantMat = GestorAulas.getAulasSubutilizadasYpromedio(solucion.getAulas())[2];
		
		assertEquals(1, aulasSub, 0);
		assertEquals(3, horasAula, 0);
		assertEquals(3, cantMat, 0);
	}
	
	@Test
	public void testGetPromedioMateriasAulas() 
	{
		Subconjunto solucion = new Subconjunto();
		ListadoMaterias listado = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 12 , 13);
		Materia materia3 = new Materia ("c", 9 , 10);
		double promedioMateriasAulas;
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		solucion.getAulas().add(listado);
		
		promedioMateriasAulas = GestorAulas.getPromedioMateriasAulas(solucion.getAulas());
		
		assertEquals(3, promedioMateriasAulas, 0);
	}
	
	@Test
	public void testGetInfoAulas() 
	{
		ListadoMaterias listado = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 12 , 13);
		Materia materia3 = new Materia ("c", 9 , 10);
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		
		assertEquals(3, GestorAulas.getInfoAula(listado)[0], 0);
		assertEquals(11, GestorAulas.getInfoAula(listado)[1], 0);
	}
	
	@Test
	public void testIntercambiarMaterias() 
	{
		ListadoMaterias listado = new ListadoMaterias();
		ListadoMaterias listado2 = new ListadoMaterias();
		Materia materia = new Materia ("a", 8 , 9);
		Materia materia2 = new Materia ("b", 12 , 13);
		Materia materia3 = new Materia ("c", 9 , 10);
		
		listado.addMateria(materia);
		listado.addMateria(materia2);
		listado.addMateria(materia3);
		
		listado2.addMateria(materia3);
		
		assertTrue(GestorAulas.intercambiarMaterias(materia, listado, listado2));
		assertFalse(GestorAulas.intercambiarMaterias(materia3, listado, listado2));
		assertEquals(2, listado2.getNumeroMaterias());
	}
}
