package Test_AG;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Logica_AG.GeneradorPrefijado;
import Logica_AG.Individuo;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Instancia;

public class IndividuoTest 
{
	private Instancia instancia;
	
	@Before
	public void inicializar()
	{
		instancia = new Instancia();
	}
	
	@Test
	public void testMutar() 
	{
		ListadoMaterias materias = new ListadoMaterias();
	
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 9 , 10));
		
		instancia.setMaterias(materias);
		
		testearMutacion("223", "023");
	}
	
	@Test
	public void testMutarSinvecinos() 
	{
		ListadoMaterias materias = new ListadoMaterias();
		
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 11 , 12));
		materias.addMateria(new Materia("c", 9 , 10));
		
		instancia.setMaterias(materias);
		
		testearMutacion("123", "123");
	}
	
	@Test
	public void recombinarTest()
	{
		ListadoMaterias materias = new ListadoMaterias();
		
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 11 , 12));
		materias.addMateria(new Materia("c", 9 , 10));
		materias.addMateria(new Materia("d", 9 , 10));
		
		instancia.setMaterias(materias);
		
		testearRecombinacion("1234", "5678", 2, "1278", "5634");
		testearRecombinacion("1234", "5678", 1, "1678", "5234");
	}
	
	@Test
	public void testFitness()
	{
		ListadoMaterias materias = new ListadoMaterias();
		Individuo ind;
		
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 11 , 12));
		materias.addMateria(new Materia("c", 9 , 10));
		materias.addMateria(new Materia("d", 9 , 10));
		
		instancia.setMaterias(materias);
		
		GeneradorPrefijado generador = new GeneradorPrefijado(5);
		
		Individuo.setGenerador(generador);
		ind = Individuo.aleatorio(instancia, 5);
		
		assertEquals(2, ind.fitness(), 0);
	}

	
	private void testearRecombinacion(String padre1, String padre2, int punto, String hijo1, String hijo2)
	{
		GeneradorPrefijado generador = new GeneradorPrefijado(punto);
		
		Individuo.setGenerador(generador);
		Individuo padre = Individuo.aleatorio(instancia, punto);
		Individuo madre = Individuo.aleatorio(instancia, punto);
		
		forzarColor(padre, convertir(padre1));
		forzarColor(madre, convertir(padre2));
		
		Individuo[] hijos = padre.recombinar(madre);
		
		assertIguales(convertir(hijo1), hijos[0]);
		assertIguales(convertir(hijo2), hijos[1]);
	}
	
	private void testearMutacion(String original, String resultado)
	{
		GeneradorPrefijado generador = new GeneradorPrefijado(0);

		Individuo.setGenerador(generador);
		Individuo individuo = Individuo.aleatorio(instancia, 0);
		
		forzarColor(individuo, convertir(original));
		
		individuo.mutar();
		
		assertIguales(convertir(resultado), individuo);
	}
	
	private void assertIguales(int[] mutados, Individuo individuo)
	{
		for(int i = 0; i < mutados.length ; ++i)
			assertEquals(mutados[i], individuo.get(i));
	}
	
	private void forzarColor(Individuo ind, int[] colores)
	{
		for (int color = 0 ; color < colores.length ; color++) 
			ind.set(color, colores[color]);
	}
	
	private int[] convertir(String numeros)
	{
		int[] ret = new int[numeros.length()];

		for (int num = 0 ; num < numeros.length() ; num++)
			ret[num] = Character.getNumericValue(numeros.charAt(num));

		return ret;
	}
}
