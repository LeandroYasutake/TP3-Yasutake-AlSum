package Test_AG;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import Logica_AG.GeneradorRandom;
import Logica_AG.Individuo;
import Logica_AG.Poblacion;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Instancia;

public class PoblacionTest 
{
	private Instancia instancia;
	private ListadoMaterias materias;
	
	@Before
	public void inicializar()
	{
		instancia = new Instancia();
		materias = new ListadoMaterias();
	}
	
	@Test
	public void testCrearPoblacion()
	{
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 9 , 10));
		
		instancia.setMaterias(materias);
		
		Poblacion poblacion = new Poblacion(instancia, 2);
		
		assertEquals(2, poblacion.getAulasDeseadas(), 0);
		assertEquals(0, poblacion.getObservadores().size(), 0);
	}
	
	@Test
	public void testSimular()
	{
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 9 , 10));
		
		instancia.setMaterias(materias);
		
		Poblacion.setGenerador(new GeneradorRandom());
		Individuo.setGenerador(new GeneradorRandom());
		Poblacion poblacion = new Poblacion(instancia, 2);
		
		assertIguales(new int[] {0,1}, poblacion.simular().getColoresAsignados());
	}
	
	@Test
	public void testSimularBorde1()
	{
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 9 , 10));
		materias.addMateria(new Materia("c", 11 , 12));
		
		instancia.setMaterias(materias);
		
		Poblacion.setGenerador(new GeneradorRandom());
		Individuo.setGenerador(new GeneradorRandom());
		Poblacion poblacion = new Poblacion(instancia, 1);
		
		assertIguales(new int[] {0}, poblacion.simular().getColoresAsignados());
	}
	
	@Test
	public void testSimularBorde2()
	{
		materias.addMateria(new Materia("a", 8 , 9));
		materias.addMateria(new Materia("b", 8 , 9));
		materias.addMateria(new Materia("c", 8 , 9));
		
		instancia.setMaterias(materias);
		
		Poblacion.setGenerador(new GeneradorRandom());
		Individuo.setGenerador(new GeneradorRandom());
		Poblacion poblacion = new Poblacion(instancia, 3);
		
		assertIguales(new int[] {0,1,2}, poblacion.simular().getColoresAsignados());
	}

	private void assertIguales(int[] coloresAutilizar, int[] coloresAsignados) 
	{
		for (int color : coloresAutilizar)
			assertTrue(contains(coloresAsignados, color));
	}

	private boolean contains(int[] colores, int color) 
	{
		Arrays.sort(colores);
		int index = Arrays.binarySearch(colores, color);
		return index >= 0;
	}
}
