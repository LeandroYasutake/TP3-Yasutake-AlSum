package Presentacion_Main;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComboBox;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;

public class Graficador 
{
	public static boolean esNombreValido(String nombre)
	{
		Pattern patternNombre = Pattern.compile("");
		Matcher matcherNombre = patternNombre.matcher(nombre);
		
		if (matcherNombre.matches())
			return false;
		
		else
			return true;
	}
	
	public static boolean esInicioValido(String inicio)
	{
		Pattern pattern24hsInicio = Pattern.compile("(0[8-9]|2[0-1]|[8-9]|1[0-9])");
		Matcher matcherHoraInicio = pattern24hsInicio.matcher(inicio);
		
		if (!matcherHoraInicio.matches())
			return false;
		
		else
			return true;
	}
	
	public static boolean esFinValido(String fin, String inicio)
	{
		Pattern pattern24hsFin = Pattern.compile("(09|2[0-2]|9|1[0-9])");
		Matcher matcherHoraFin = pattern24hsFin.matcher(fin);
		
		if (!matcherHoraFin.matches())
			return false;
		
		else if (Integer.valueOf(fin) <= Integer.valueOf(inicio))
		{
			return false;
		}
		
		else
			return true;
	}
	
	public static boolean esAulasValida(String inicio)
	{
		Pattern patternAulas = Pattern.compile("[1-9]\\d*");
		Matcher matcherAulas = patternAulas.matcher(inicio);
		
		if (!matcherAulas.matches())
			return false;
		
		else
			return true;
	}
	
	public static void llenarComboBoxAula(JComboBox<ComboItem> comboBox, ArrayList<ListadoMaterias> aulas)
	{	
		for (int aula = 0 ; aula < aulas.size() ; aula++)
		{
			ComboItem aulaCombo = new ComboItem(aulas.get(aula), aula);
			comboBox.addItem(aulaCombo);
		}
	}
	
	public static void llenarComboBoxMateria(JComboBox<Materia> comboBox, ArrayList<Materia> materias)
	{
		Collections.sort(materias, (m1, m2) -> m1.getNombre().compareTo(m2.getNombre()));
		
		for (Materia materia : materias)
			comboBox.addItem(materia);
	}
	
	public static String getRedondeado (double num)
	{
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);
		
		return df.format(num);
	}
}
