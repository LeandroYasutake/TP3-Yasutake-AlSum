package Presentacion_Main;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import Datos.Persistidor;
import Logica_Solver.Instancia;
import Logica_Solver.Subconjunto;
import Presentacion_Tabla.TablaHorarios;
import Presentacion_VentanasListadoAsignado.VentanaCambiosManuales;
import Presentacion_VentanasListadoAsignado.VentanaInformacionAulas;
import Presentacion_VentanasListados.VentanaBuscarListado;
import Presentacion_VentanasListados.VentanaCrearListado;
import Presentacion_VentanasListados.VentanaEditarListado;
import Presentacion_VentanasListados.VentanaElegirAlgoritmo;

import javax.swing.JButton;

public class MainForm extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private MainForm MainFormPadre;
	private VentanaCrearListado ventanaCrearListado;
	private VentanaEditarListado ventanaEditarListado;
	private VentanaBuscarListado ventanaBuscarListado;
	private VentanaElegirAlgoritmo ventanaElegirAlgoritmo;
	private VentanaInformacionAulas ventanaInformacionAulas;
	private VentanaCambiosManuales ventanaIntercambiarMaterias;
	private VentanaAyuda ventanaAyuda;
	private Instancia instancia;
	private Subconjunto solucion;
	private JButton btnCrearListado;
	private JButton btnBuscarListado;
	private JButton btnEditarListado;
	private JButton btnAsignarAulas;
	private JButton btnInformacion;
	private JButton btnCambiosManuales;
	private JButton btnExportarExcel;
	private JButton btnAyuda;
	private JButton btnSalir;
	private JScrollPane scrollTabla;
	private JLabel imagenPresentacion;
	private TablaHorarios tablaAulas;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					MainForm frame = new MainForm();
					frame.setVisible(true);
					UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() 
	{
		inicializarVariables();
		inicializarPantalla();
	}

	private void inicializarVariables() 
	{
		MainFormPadre = this;
		instancia = new Instancia();
		solucion = new Subconjunto();
	}

	private void inicializarPantalla() 
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 970, 700);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 229, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Trabajo Pr�ctico N� 3: Al SUM de la biblioteca");

		setButtons();
		setTable();
	}

	private void setButtons() 
	{
		btnCrearListado = new JButton("Crear Listado");
		btnCrearListado.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/add_window_1-5.png")));
		btnCrearListado.setBounds(47, 11, 182, 57);
		btnCrearListado.setMargin(new Insets(0,0,0,0));
		btnCrearListado.setFocusable(false);
		contentPane.add(btnCrearListado);
		
		btnBuscarListado = new JButton("Buscar Listado");
		btnBuscarListado.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/search-5.png")));
		btnBuscarListado.setBounds(276, 11, 182, 57);
		btnBuscarListado.setMargin(new Insets(0,0,0,0));
		btnBuscarListado.setFocusable(false);
		contentPane.add(btnBuscarListado);
		
		btnEditarListado = new JButton("Editar Listado");
		btnEditarListado.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/advanced_options-5.png")));
		btnEditarListado.setBounds(505, 11, 182, 57);
		btnEditarListado.setMargin(new Insets(0,0,0,0));
		btnEditarListado.setFocusable(false);
		contentPane.add(btnEditarListado);
		
		btnAsignarAulas = new JButton("Asignar Aulas");
		btnAsignarAulas.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/forward___next-5.png")));
		btnAsignarAulas.setBounds(734, 11, 182, 57);
		btnAsignarAulas.setMargin(new Insets(0,0,0,0));
		btnAsignarAulas.setFocusable(false);
		contentPane.add(btnAsignarAulas);
		
		btnInformacion = new JButton("Estad\u00EDsticas");
		btnInformacion.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/fonts_2-5.png")));
		btnInformacion.setBounds(47, 90, 182, 57);
		btnInformacion.setMargin(new Insets(0,0,0,0));
		btnInformacion.setFocusable(false);
		contentPane.add(btnInformacion);
		
		btnCambiosManuales = new JButton("Cambios Manuales");
		btnCambiosManuales.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/general_options-5.png")));
		btnCambiosManuales.setBounds(276, 90, 182, 57);
		btnCambiosManuales.setMargin(new Insets(0,0,0,0));
		btnCambiosManuales.setFocusable(false);
		contentPane.add(btnCambiosManuales);
		
		btnExportarExcel = new JButton("Guardar Excel");
		btnExportarExcel.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/save-5.png")));
		btnExportarExcel.setBounds(505, 90, 182, 57);
		btnExportarExcel.setMargin(new Insets(0,0,0,0));
		btnExportarExcel.setFocusable(false);
		contentPane.add(btnExportarExcel);
		
		btnAyuda = new JButton("Ayuda");
		btnAyuda.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/java/swing/plaf/windows/icons/Question.gif")));
		btnAyuda.setBounds(734, 90, 85, 57);
		btnAyuda.setMargin(new Insets(0,0,0,0));
		btnAyuda.setFocusable(false);
		contentPane.add(btnAyuda);
		
		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(MainForm.class.getResource("/Iconos/stop_2-5.png")));
		btnSalir.setBounds(831, 90, 85, 57);
		btnSalir.setMargin(new Insets(0,0,0,0));
		btnSalir.setFocusable(false);
		contentPane.add(btnSalir);
		
		setButtonsListeners();
	}

	private void setButtonsListeners() 
	{
		btnCrearListado.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaCrearListado = new VentanaCrearListado();
            	ventanaCrearListado.setVisible(true);
            	ventanaCrearListado.setLocationRelativeTo(null);
            }
        });
		
		btnBuscarListado.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaBuscarListado = new VentanaBuscarListado(instancia, MainFormPadre);
            	ventanaBuscarListado.setVisible(true);
            	ventanaBuscarListado.setLocationRelativeTo(null);
            }
        });
		
		btnEditarListado.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (instancia.getMaterias().isEmpty() && solucion.getAulas().isEmpty())
                	JOptionPane.showMessageDialog(null, "         Listado no cargado!\r\n");
            		
            		
                else
                {
                	ventanaEditarListado = new VentanaEditarListado(instancia.getListadoMaterias());
                	ventanaEditarListado.setVisible(true);
                	ventanaEditarListado.setLocationRelativeTo(null);
                }
            }
        });
		
		btnAsignarAulas.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (instancia.getMaterias().isEmpty() && solucion.getAulas().isEmpty())
            		JOptionPane.showMessageDialog(null, "         Listado no cargado!\r\n");
                	
            	else
                {
            		ventanaElegirAlgoritmo = new VentanaElegirAlgoritmo(MainFormPadre);
            		ventanaElegirAlgoritmo.setVisible(true);
            		ventanaElegirAlgoritmo.setLocationRelativeTo(null);
                }
            }
        });
		
		btnInformacion.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (!solucion.getAulas().isEmpty())
            	{
            		ventanaInformacionAulas = new VentanaInformacionAulas(solucion, ventanaBuscarListado.getListadoBuscado());
                	ventanaInformacionAulas.setVisible(true);
                	ventanaInformacionAulas.setLocationRelativeTo(null);
            	}
            	
            	else
            		JOptionPane.showMessageDialog(null, "         Listado no asignado!\r\n");
            }
        });
		
		btnCambiosManuales.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (!solucion.getAulas().isEmpty())
            	{
            		ventanaIntercambiarMaterias = new VentanaCambiosManuales(MainFormPadre);
            		ventanaIntercambiarMaterias.setVisible(true);
            		ventanaIntercambiarMaterias.setLocationRelativeTo(null);
            	}
            	
            	else
            		JOptionPane.showMessageDialog(null, "         Listado no asignado!\r\n");
            }
        });
		
		btnExportarExcel.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (!solucion.getAulas().isEmpty())
            	{
            		JFileChooser chooser = new JFileChooser();
            	    int retrival = chooser.showSaveDialog(null);
            	    
            	    if (retrival == JFileChooser.APPROVE_OPTION)
            	    {
            	    	Persistidor.toExcel(chooser.getSelectedFile()+".xls", tablaAulas.getModel().getModelo());
            	    	JOptionPane.showMessageDialog(null, "         Listado exportado!\r\n");
            	    }
            	}
            	
            	else
            		JOptionPane.showMessageDialog(null, "         Listado no asignado!\r\n");
            }
        });
		
		btnAyuda.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				ventanaAyuda = new VentanaAyuda();
				ventanaAyuda.setVisible(true);
				ventanaAyuda.setLocationRelativeTo(null);
			}
		});
		
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
	}

	public void cargarTabla() 
	{
		tablaAulas.updateModel(solucion.getAulas());
		scrollTabla.setViewportView(tablaAulas.getTable());
	}

	private void setTable() 
	{
		tablaAulas = new TablaHorarios();
		
		scrollTabla = new JScrollPane();
		scrollTabla.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollTabla.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollTabla.setBounds(20, 156, 924, 502);
		imagenPresentacion = new JLabel(new ImageIcon(MainForm.class.getResource("/Iconos/Presentacion.png")));
		imagenPresentacion.setLocation(31, 0);
		scrollTabla.setViewportView(imagenPresentacion);
		getContentPane().add(scrollTabla);
	}
	
	public Instancia getInstancia()
	{
		return this.instancia;
	}
	
	public Instancia setInstancia(Instancia inst)
	{
		return this.instancia = inst;
	}

	public Subconjunto getSolucion() 
	{
		return this.solucion;
	}
}
