package Presentacion_Main;

import Logica_Materias.ListadoMaterias;

public class ComboItem 
{
	private ListadoMaterias aula;
	private int posicion;
	
	public ComboItem (ListadoMaterias a, int pos)
	{
		this.aula = a;
		this.posicion = pos+1;
	}
	
	 public ListadoMaterias getValue() 
	 {
	        return this.aula;
	 }

    @Override
    public String toString() 
    {
        return "Aula "+posicion;
    }
}
