package Presentacion_Main;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JButton;

import java.awt.SystemColor;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class VentanaAyuda extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private String texto1;
	private String texto2;
	private String texto3;
	private String texto4;
	private String[] textos;
	private JTextArea txtr;
	private JButton btnAnterior;
	private JButton btnSalir;
	private JButton btnSiguiente;
	private int posicion = 0;

	public VentanaAyuda() 
	{
		setResizable(false);
		setBounds(100, 100, 450, 350);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		setTitle("Ayuda");
		contentPanel.setLayout(null);
		
		setStrings();
		setJText();
		setButtons();
	}

	private void setStrings() 
	{
		texto1 = "Aplicaci\u00F3n: Mediante este sistema, podr\u00E1 asignar aulas a materias. La aplicaci\u00F3n tendr\u00E1 en cuenta "
				+ "el horario de cada materia a modo de no existir solapamientos entre estas para una correcta visualizaci\u00F3n de la "
				+ "asignaci\u00F3n. \r\n\r\nListados de materias: Estos contienen las materias que ser\u00E1n utilizadas para la asignaci\u00F3n "
				+ "mediante \r\naulas. Podr\u00E1 crear uno nuevo o cargar otro \r\npreexistente. A su vez, luego podr\u00E1 modificarlo a \r\ngusto "
				+ "antes de la asignaci\u00F3n si as\u00ED lo desea. Por \r\n\u00FAltimo, estos listados puede ser guardados en el \r\nsistema mismo o en otra locaci\u00F3n deseada.";
		
		texto2 = "Asignaci\u00F3n de Materias: Con \u00E9sta funci\u00F3n cuenta con tres modos de asignaci\u00F3n. El primero selecciona de forma golosa las materias inmediatamente "
				+ "compatibles. El segundo modo representa las\r\nmaterias en un grafo para luego ser coloreado de forma golosa y as\u00ED obtener la asignaci\u00F3n de aulas a materias "
				+ "sin conflictos de horarios. Por \u00FAltimo, el problema puede resolverse mediante un algoritmo gen\u00E9tico evolutivo aplicado a la coloraci\u00F3n de grafos "
				+ "seleccionando el n\u00FAmero de aulas deseadas. Adem\u00E1s puede observar el progreso en tiempo real. ";
		
		texto3 = "Informaci\u00F3n: Aqu\u00ED podr\u00E1 visualizar datos del listado ya asignado. Por ejemplo, aulas subutilizadas (aulas con un n\u00FAmero de horas ocupadas menor a 6hs), promedios, "
				+ "aulas utilizadas, etc. Por \u00FAltimo, puede visualizar estos datos en gr\u00E1ficos.\r\n\r\nCambios Manuales: Aqu\u00ED podr\u00E1 realizar intercambios de materias entre aulas (avisar\u00E1 "
				+ "la existencia de conflictos de horarios) o bien realizar operaciones de quita o agregado de elementos tales como materias y aulas sobre el listado ya asignado.\r\n";
		
		texto4 = "Guardar Excel: Con esta opci\u00F3n podr\u00E1 exportar el resultado de la asignaci\u00F3n a un archivo compatible (.xls) con Microsoft Excel.\r\n\r\nTrabajo realizado por Leandro Yasutake. UNGS."
				+ "\r\n\r\nProfesores: Patricia Bagnes - Alejandro Nelis.";
		
		textos = new String[] {texto1, texto2, texto3, texto4};
	}

	private void setJText() 
	{
		txtr = new JTextArea();
		txtr.setWrapStyleWord(true);
		txtr.setFont(new Font("Monospaced", Font.BOLD, 13));
		txtr.setText(texto1);
		txtr.setBackground(SystemColor.control);
		txtr.setLineWrap(true);
		txtr.setEnabled(false);
		txtr.setEditable(false);
		txtr.setBounds(10, 11, 414, 245);
		contentPanel.add(txtr);
	}

	private void setButtons() 
	{
		btnAnterior = new JButton("Anterior");
		btnAnterior.setIcon(new ImageIcon(VentanaAyuda.class.getResource("/Iconos/back_2-7.png")));
		btnAnterior.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (posicion > 0)
				{
					posicion--;
					txtr.setText(textos[posicion]);
					
				}
			}
		});
		btnAnterior.setBounds(10, 278, 107, 23);
		contentPanel.add(btnAnterior);
		
		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(VentanaAyuda.class.getResource("/Iconos/stop_2-7.png")));
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		btnSalir.setBounds(171, 278, 107, 23);
		contentPanel.add(btnSalir);
		
		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setIcon(new ImageIcon(VentanaAyuda.class.getResource("/Iconos/forward_2-7.png")));
		btnSiguiente.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (posicion < 3)
				{
					posicion++;
					txtr.setText(textos[posicion]);
				}
			}
		});
		btnSiguiente.setBounds(327, 278, 107, 23);
		contentPanel.add(btnSiguiente);
	}
}
