package Logica_AG;

import Logica_Grafo.Grafo;
import Logica_Solver.Instancia;

public class Individuo
{
	private int[] colores;
	private Instancia instancia;
	private Grafo grafoMaterias;
	private static Generador random = null;
	private int coloresAutilizar;
	
	public static void setGenerador(Generador generador)
	{
		random = generador;
	}

	public static Individuo aleatorio(Instancia instancia, int coloresDeseados)
	{
		Individuo ret = new Individuo(instancia, coloresDeseados);
		
		for(int i = 0; i < instancia.getMaterias().size() ; ++i)
			ret.set(i, random.nextInt(coloresDeseados));
		
		return ret;
	}
	
	public static Individuo vacio(Instancia instancia, int coloresDeseados)
	{
		return new Individuo(instancia, coloresDeseados);
	}

	private Individuo(Instancia instancia, int coloresDeseados)
	{
		this.colores = new int[instancia.getMaterias().size()];
		this.grafoMaterias = instancia.getListadoMaterias().crearGrafo();
		this.instancia = instancia;
		this.coloresAutilizar = coloresDeseados;
	}

	public void mutar()
	{
		int verticeRandom = random.nextInt(colores.length);
		
		//colores[verticeRandom] = random.nextInt(coloresAutilizar); dos metodos de mutacion
		
		for (int vecino : grafoMaterias.vecinos(verticeRandom))
			if (colores[verticeRandom] == colores[vecino])
			{
				int colorNuevo = random.nextInt(coloresAutilizar);
				set(verticeRandom, colorNuevo);
			}
	}
	
	public Individuo[] recombinar(Individuo otro)
	{
		Individuo hijo1 = Individuo.vacio(instancia, coloresAutilizar);
		Individuo hijo2 = Individuo.vacio(instancia, coloresAutilizar);
		
		int corte = random.nextInt(colores.length);
		
		for(int i = 0 ; i < corte ; ++i)
		{
			hijo1.set(i, this.get(i));
			hijo2.set(i, otro.get(i));
		}
		
		for(int i = corte ; i < colores.length ; ++i)
		{
			hijo1.set(i, otro.get(i));
			hijo2.set(i, this.get(i));
		}
		
		return new Individuo[] {hijo1, hijo2};
	}
	
	public double fitness()
	{
		double conflicto = 0;
		
		for (int vertice = 0 ; vertice < grafoMaterias.cantidadDeVertices() ; vertice++)
			for (int vecino : grafoMaterias.vecinos(vertice))
				if (colores[vertice] == colores[vecino])
					conflicto++;
		
		return conflicto;
	}
	
	public int get(int i)
	{
		return colores[i];
	}
	
	public void set(int i, int color)
	{
		colores[i] = color;
	}
	
	public int[] getColoresAsignados()
	{
		return this.colores;
	}
}
