package Logica_AG;

import java.util.Random;

public class GeneradorRandom implements Generador
{
	private Random random;
	
	public GeneradorRandom()
	{
		random = new Random();
	}

	public int nextInt(int valor) 
	{
		return random.nextInt(valor);
	}
}
