package Logica_AG;

public class GeneradorPrefijado implements Generador
{
	private int entero;

	
	public GeneradorPrefijado(int enteroPrefijado)
	{
		this.entero = enteroPrefijado;
	}

	public int nextInt(int limite) 
	{
		return this.entero;
	}
}
