package Logica_AG;

public interface Observador
{
	public void notificar();
}
