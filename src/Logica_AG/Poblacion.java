package Logica_AG;

import java.util.ArrayList;
import java.util.Collections;

import Logica_Solver.Instancia;

public class Poblacion
{
	private Instancia instancia;
	private ArrayList<Individuo> individuos;
	private int coloresDeseados;
	private int tama�o = 100;
	private int mutadosPorIteracion = 20;
	private int recombinacionesPorIteracion = 20;
	private int eliminadosPorIteracion = 50;
	private ArrayList<Observador> observadores;
	private static Generador random = null;
	
	public static void setGenerador(Generador generador)
	{
		random = generador;
	}
	
	public void registrar(Observador observador)
	{
		this.observadores.add(observador);
	}

	public Poblacion(Instancia instancia, int aulasDeseadas)
	{
		this.instancia = instancia;
		this.coloresDeseados = aulasDeseadas;
		this.observadores = new ArrayList<Observador>();
	}
	
	public void registrarObservador(Observador observador)
	{
		this.observadores.add(observador);
	}

	public Individuo simular()
	{
		inicializarAleatoriamente();
		notificarObservadores();

		while(!satisfactoria())
		{
			mutarAlgunos();
			recombinarAlgunos();
			eliminarPeores();
			completarConAleatorios();
			notificarObservadores();
		}
		
		return mejorIndividuo();
	}

	private void inicializarAleatoriamente()
	{
		individuos = new ArrayList<Individuo>();
		
		for(int i = 0 ; i < tama�o ; ++i)
			individuos.add(Individuo.aleatorio(instancia, coloresDeseados));
	}

	private boolean satisfactoria()
	{
		if (mejorIndividuo().fitness() == 0)
			return true;
		
		return false;
	}

	private void mutarAlgunos()
	{
		for(int i = 0 ; i < mutadosPorIteracion ; ++i)
		{
			Individuo seleccionado = aleatorio();
			seleccionado.mutar();
		}		
	}

	private void recombinarAlgunos()
	{
		for(int i = 0; i < recombinacionesPorIteracion ; ++i)
		{
			Individuo padre1 = aleatorio();
			Individuo padre2 = aleatorio();
			
			for(Individuo hijo : padre1.recombinar(padre2))
				individuos.add(hijo);
		}
	}

	private Individuo aleatorio()
	{
		int j = random.nextInt(tama�o);
		
		return individuos.get(j);
	}

	private void eliminarPeores()
	{
		ordenarPorFitness();
		
		for(int i = 0 ; i < eliminadosPorIteracion ; ++i)
			individuos.remove(individuos.size()-1);
	}

	private void completarConAleatorios()
	{
		while(individuos.size() < tama�o)
			individuos.add(Individuo.aleatorio(instancia, coloresDeseados));
	}
	
	private void notificarObservadores()
	{
		for(Observador observador : observadores)
			observador.notificar();
	}

	public Individuo mejorIndividuo()
	{
		ordenarPorFitness();
		
		return individuos.get(0);
	}

	public Individuo peorIndividuo()
	{
		ordenarPorFitness();
		
		return individuos.get( individuos.size()-1 );
	}

	private void ordenarPorFitness()
	{
		Collections.sort(individuos, (i1,i2) -> (int)(i1.fitness() - i2.fitness()));
	}

	public double fitnessPromedio()
	{
		double suma = 0;
		
		for(Individuo individuo : individuos)
			suma += individuo.fitness();
		
		return suma / individuos.size();
	}
	
	public int getAulasDeseadas()
	{
		return this.coloresDeseados;
	}
	
	public ArrayList<Observador> getObservadores()
	{
		return this.observadores;
	}
}
