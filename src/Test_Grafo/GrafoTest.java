package Test_Grafo;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import Logica_Grafo.Grafo;

public class GrafoTest 
{
	@Test
	public void grafo2Test()
	{
		Grafo miGrafo = new Grafo(2);
		assertEquals(2, miGrafo.cantidadDeVertices());
	}
	
	@Test
	public void grafo0Test()
	{
		Grafo miGrafo = new Grafo(0);
		assertEquals(0, miGrafo.cantidadDeVertices());
	}

	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void grafoVerticesIlegalesTest()
	{
		Grafo miGrafo = new Grafo(-1);
	}
	
	@Test
	public void agregarAristaSimple()
	{
		Grafo grafo = new Grafo(3);
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);
		
		assertEquals(2, grafo.cantidadDeAristas());
		assertTrue(grafo.contieneArista(0, 1));
		assertTrue(grafo.contieneArista(1, 0));
		assertTrue(grafo.contieneArista(0, 2));
		assertTrue(grafo.contieneArista(2, 0));
		assertFalse(grafo.contieneArista(1, 2));
		assertFalse(grafo.contieneArista(2, 1));
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaVerticeInexistente()
	{
		Grafo miGrafo = new Grafo(3);
	
		miGrafo.agregarArista(0, 1);
		miGrafo.agregarArista(3, 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaVerticenNegativo()
	{
		Grafo miGrafo = new Grafo(3);
	
		miGrafo.agregarArista(0, 1);
		miGrafo.agregarArista(-1, 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarLoop()
	{
		Grafo miGrafo = new Grafo(3);
	
		miGrafo.agregarArista(0, 0);
	}
	
	@Test
	public void removerArista()
	{
		Grafo grafo = new Grafo(3);
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);
		
		grafo.removerArista(0, 1);
		
		assertEquals(1, grafo.cantidadDeAristas());
		assertFalse(grafo.contieneArista(0, 1));
		assertFalse(grafo.contieneArista(1, 0));
		assertTrue(grafo.contieneArista(0, 2));
		assertTrue(grafo.contieneArista(2, 0));
	}
	
	@Test
	public void grado()
	{
		Grafo grafo = new Grafo(5);
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);
		grafo.agregarArista(3, 0);

		assertEquals(3, grafo.grado(0));
		assertEquals(1, grafo.grado(1));
		assertEquals(1, grafo.grado(2));
		assertEquals(1, grafo.grado(3));
		assertEquals(0, grafo.grado(4));
	}

	@Test
	public void aristas()
	{
		Grafo grafo = new Grafo(5);
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);
		grafo.agregarArista(3, 0);

		assertEquals(3, grafo.cantidadDeAristas());
	}
	
	@Test
	public void vecinos()
	{
		Grafo grafo = new Grafo(5);
		Set<Integer> primero = new HashSet<Integer>();
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);
		grafo.agregarArista(3, 0);
		
		primero.add(1);
		primero.add(2);
		primero.add(3);
		
		assertEquals(primero, grafo.vecinos(0));
	}

	@Test
	public void sonVecinos()
	{
		Grafo grafo = new Grafo(5);
		
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);
		grafo.agregarArista(3, 0);

		assertTrue(grafo.sonVecinos(0, 1));
		assertTrue(grafo.sonVecinos(0, 2));
		assertTrue(grafo.sonVecinos(0, 3));
		assertFalse(grafo.sonVecinos(0, 4));
		assertEquals(3,grafo.vecinos(0).size());
	}

}
