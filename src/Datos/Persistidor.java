package Datos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.table.DefaultTableModel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Logica_Materias.ListadoMaterias;

public class Persistidor 
{
	public static void generarJSON(ListadoMaterias listadoMaterias, String rutaArchivo)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(listadoMaterias);
		
		try
		{
			FileWriter writer = new FileWriter(rutaArchivo);
			writer.write(json);
			writer.close();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static ListadoMaterias leerJSON(String rutaArchivo)
	{
		Gson gson = new Gson();
		ListadoMaterias ret = null;

		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(rutaArchivo));
			ret = gson.fromJson(br, ListadoMaterias.class);
		} 
		catch (IOException e){}
		
		return ret;
	}
	
	public static void toExcel(String rutaArchivo, DefaultTableModel model)
	{
	    try
	    {
	        FileWriter excel = new FileWriter(rutaArchivo);
	
	        for(int columna = 0 ; columna < model.getColumnCount() ; columna++)
	            excel.write(model.getColumnName(columna) + "\t");
	
	        excel.write("\n");
	
	        for(int fila = 0 ; fila < model.getRowCount(); fila++) 
	        {
	            for(int columna = 0; columna < model.getColumnCount(); columna++)
	            {
	            	if (model.getValueAt(fila,columna) != null)
	            		excel.write(model.getValueAt(fila,columna)+"\t");
	            	
	            	else
	            		excel.write("           -"+"\t");
	            }
	            excel.write("\n");
	        }
	
	        excel.close();
	    }
	    catch(IOException e)
	    {
	    	System.out.println(e);
	    }
	}
}
