package Presentacion_VentanasListados;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Presentacion_Main.Graficador;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class VentanaCrearListado extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel tituloMateria;
	private JLabel materiaNombre;
	private JLabel horaInicioLabel;
	private JLabel horaFinLabel;
	private JLabel lblCantidadDeMaterias;
	private JLabel lblNēMaterias;
	private JTextField nombreMateriaInput;
	private JTextField horaInicioInput;
	private JTextField horaFinInput;
	private JButton buttonAgregarMateria;
	private JButton buttonCancelar;
	private JButton buttonGuardarListado;
	private ListadoMaterias materiasSinAula;
	private VentanaCrearListado ventanaPadre;
	private VentanaGuardarListado ventanaGuardarListado;
	
	public VentanaCrearListado() 
	{
		setResizable(false);
		setBounds(100, 100, 630, 325);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		contentPanel.setLayout(null);
		setTitle("Crear listado de materias");
		
		materiasSinAula = new ListadoMaterias();
		ventanaPadre = this;
		
		setLabels();
		setTextFields();
		setButtons();
	}

	private void setLabels() 
	{
		tituloMateria = new JLabel("Indique la materia a agregar y el nombre del listado:");
		tituloMateria.setFont(new Font("Tahoma", Font.BOLD, 14));
		tituloMateria.setBounds(133, 11, 358, 23);
		contentPanel.add(tituloMateria);
		
		materiaNombre = new JLabel("Materia:");
		materiaNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		materiaNombre.setBounds(10, 67, 54, 14);
		contentPanel.add(materiaNombre);
		
		horaInicioLabel = new JLabel("Hora de inicio (24hs):");
		horaInicioLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaInicioLabel.setBounds(10, 113, 124, 14);
		contentPanel.add(horaInicioLabel);
		
		horaFinLabel = new JLabel("Hora de finalizaci\u00F3n (24hs):");
		horaFinLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaFinLabel.setBounds(10, 165, 164, 14);
		contentPanel.add(horaFinLabel);
		

		lblCantidadDeMaterias = new JLabel("Cantidad de materias:");
		lblCantidadDeMaterias.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCantidadDeMaterias.setBounds(10, 212, 144, 14);
		contentPanel.add(lblCantidadDeMaterias);
		
		lblNēMaterias = new JLabel("0");
		lblNēMaterias.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNēMaterias.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNēMaterias.setBounds(568, 213, 46, 14);
		contentPanel.add(lblNēMaterias);
	}
	
	private void setTextFields() 
	{
		nombreMateriaInput = new JTextField();
		nombreMateriaInput.setHorizontalAlignment(SwingConstants.CENTER);
		nombreMateriaInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		nombreMateriaInput.setBounds(322, 65, 292, 20);
		nombreMateriaInput.setColumns(10);
		contentPanel.add(nombreMateriaInput);
		
		horaInicioInput = new JTextField();
		horaInicioInput.setHorizontalAlignment(SwingConstants.CENTER);
		horaInicioInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaInicioInput.setBounds(322, 111, 292, 20);
		horaInicioInput.setColumns(10);
		contentPanel.add(horaInicioInput);
		
		horaFinInput = new JTextField();
		horaFinInput.setHorizontalAlignment(SwingConstants.CENTER);
		horaFinInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaFinInput.setBounds(322, 163, 292, 20);
		horaFinInput.setColumns(10);
		contentPanel.add(horaFinInput);
	}
	
	private void setButtons() 
	{
		buttonAgregarMateria = new JButton("Agregar materia");
		buttonAgregarMateria.setIcon(new ImageIcon(VentanaCrearListado.class.getResource("/Iconos/add_folder-7.png")));
		buttonAgregarMateria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonAgregarMateria.setBounds(48, 263, 144, 23);
		buttonAgregarMateria.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (!Graficador.esNombreValido(nombreMateriaInput.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre correcto para la materia deseada.\r\n");
				
				else if (!Graficador.esInicioValido(horaInicioInput.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese una hora de inicio correcta (24hs).\r\n");
				
				else if (!Graficador.esFinValido(horaFinInput.getText(), horaInicioInput.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese una hora de finalizaciķn correcta (24hs).\r\n");
				
				else
				{
					Materia nuevaMateria = new Materia(nombreMateriaInput.getText(), Integer.parseInt(horaInicioInput.getText()), Integer.parseInt(horaFinInput.getText()));
					materiasSinAula.addMateria(nuevaMateria);
					lblNēMaterias.setText(""+materiasSinAula.getNumeroMaterias());
					JOptionPane.showMessageDialog(null, "Materia agregada exitosamente!\r\n");
					resetearLabels();
				}
			}
		});
		contentPanel.add(buttonAgregarMateria);
		
		buttonGuardarListado = new JButton("Guardar listado...");
		buttonGuardarListado.setIcon(new ImageIcon(VentanaCrearListado.class.getResource("/Iconos/save-7.png")));
		buttonGuardarListado.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonGuardarListado.setBounds(240, 263, 144, 23);
		buttonGuardarListado.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (materiasSinAula.getNumeroMaterias() >= 1)
				{
					ventanaGuardarListado = new VentanaGuardarListado(ventanaPadre, materiasSinAula);
					ventanaGuardarListado.setVisible(true);
					ventanaGuardarListado.setLocationRelativeTo(null);
				}
				else
					JOptionPane.showMessageDialog(null, "   No se han ingresado materias.\r\n");
			}
		});
		contentPanel.add(buttonGuardarListado);
		
		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setIcon(new ImageIcon(VentanaCrearListado.class.getResource("/Iconos/stop_2-7.png")));
		buttonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonCancelar.setBounds(432, 262, 144, 23);
		buttonCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(buttonCancelar);
	}

	protected void resetearLabels() 
	{
		nombreMateriaInput.setText("");
		horaInicioInput.setText("");
		horaFinInput.setText("");
	}
	
	public void cerrarVentana()
	{
		this.dispose();
	}
}
