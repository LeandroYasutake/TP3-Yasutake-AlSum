package Presentacion_VentanasListados;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

import Datos.Persistidor;
import Logica_Materias.ListadoMaterias;
import Presentacion_Main.Graficador;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class VentanaGuardarListado extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel titulo;
	private JLabel nombreListadoLabel;
	private JTextField nombreListadoText;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private VentanaCrearListado VentanaGuardarListadoPadre;
	private JButton btnGuardarEn;
	private ListadoMaterias listadoAguardar;

	public VentanaGuardarListado(VentanaCrearListado ventanaGuardarListado, ListadoMaterias materiasSinAula) 
	{
		setResizable(false);
		setBounds(100, 100, 450, 250);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		contentPanel.setLayout(null);
		setTitle("Guardar listado de materias");
		
		VentanaGuardarListadoPadre = ventanaGuardarListado;
		listadoAguardar = materiasSinAula;
		
		setLabels();
		setTextField();
		setButtons();
	}

	private void setLabels() 
	{
		titulo = new JLabel("Indique el nombre del listado de materias a guardar:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		titulo.setBounds(37, 11, 360, 26);
		contentPanel.add(titulo);
		
		nombreListadoLabel = new JLabel("Nombre del listado:");
		nombreListadoLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		nombreListadoLabel.setBounds(10, 102, 118, 14);
		contentPanel.add(nombreListadoLabel);
	}
	
	private void setTextField() 
	{
		nombreListadoText = new JTextField();
		nombreListadoText.setHorizontalAlignment(SwingConstants.CENTER);
		nombreListadoText.setFont(new Font("Tahoma", Font.PLAIN, 13));
		nombreListadoText.setBounds(231, 100, 193, 20);
		contentPanel.add(nombreListadoText);
		nombreListadoText.setColumns(10);
	}
	
	private void setButtons() 
	{
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(VentanaGuardarListado.class.getResource("/Iconos/save-7.png")));
		btnGuardar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnGuardar.setBounds(10, 176, 125, 23);
		btnGuardar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (!Graficador.esNombreValido(nombreListadoText.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre correcto para la materia deseada.\r\n");
				
				else
				{
					Persistidor.generarJSON(listadoAguardar, "Archivos\\"+nombreListadoText.getText()+".JSON");
					JOptionPane.showMessageDialog(null, "        Listado guardado! \r\n");
					if (VentanaGuardarListadoPadre != null)
						VentanaGuardarListadoPadre.cerrarVentana();
				}	
				
				dispose();
			}
		});
		contentPanel.add(btnGuardar);
		
		btnGuardarEn = new JButton("Guardar en...");
		btnGuardarEn.setIcon(new ImageIcon(VentanaGuardarListado.class.getResource("/Iconos/browse_1-7.png")));
		btnGuardarEn.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnGuardarEn.setBounds(159, 176, 125, 23);
		btnGuardarEn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showSaveDialog(null);
			    
				if (returnValue == JFileChooser.APPROVE_OPTION) 
			    {
					Persistidor.generarJSON(listadoAguardar, fileChooser.getSelectedFile().toString()+".JSON");
				    JOptionPane.showMessageDialog(null, "        Listado guardado! \r\n");
				    dispose();
			    }
			}
		});
		contentPanel.add(btnGuardarEn);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(VentanaGuardarListado.class.getResource("/Iconos/stop_2-7.png")));
		btnCancelar.setBounds(309, 177, 125, 23);
		btnCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(btnCancelar);
	}
}
