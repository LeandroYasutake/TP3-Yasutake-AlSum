package Presentacion_VentanasListados;

import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JTextField;

import Logica_Solver.Instancia;
import Logica_Solver.SolverGoloso;
import Logica_Solver.Subconjunto;
import Presentacion_Main.Graficador;
import Presentacion_Main.MainForm;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class VentanaGenetica extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblTitulo;
	private JLabel lblAulasMnimas; 
	private JLabel lblAulasMnimasNº; 
	private JLabel lblAulasAUtilizar; 
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JTextField textFieldAulas;
	private MainForm ventanaPadre;
	private Instancia instancia;
	private Subconjunto solucion;
	private ProcesoAsignarAulasSW procesoCostoso;
	private SolverGoloso solverAuxiliar;

	public VentanaGenetica(MainForm ventana, Instancia inst) 
	{
		setBounds(100, 100, 450, 240);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		setTitle("Parámetros");
		contentPanel.setLayout(null);
		
		this.ventanaPadre = ventana;
		this.instancia = inst;
		this.solucion = ventana.getSolucion();
		
		setLabels();
		setAulasMinimas();
		setTextField();
		setButtons();
	}
	
	private void setLabels() 
	{
		lblTitulo = new JLabel("Seleccione los par\u00E1metros para la resoluci\u00F3n:");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitulo.setBounds(63, 11, 307, 23);
		contentPanel.add(lblTitulo);
		
		lblAulasMnimas = new JLabel("Aulas m\u00EDnimas requeridas (aprox.):");
		lblAulasMnimas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAulasMnimas.setBounds(10, 58, 212, 23);
		contentPanel.add(lblAulasMnimas);
		
		lblAulasMnimasNº = new JLabel("0");
		lblAulasMnimasNº.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAulasMnimasNº.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAulasMnimasNº.setBounds(378, 63, 46, 14);
		contentPanel.add(lblAulasMnimasNº);
		
		lblAulasAUtilizar = new JLabel("Aulas a utilizar:");
		lblAulasAUtilizar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAulasAUtilizar.setBounds(10, 104, 95, 23);
		contentPanel.add(lblAulasAUtilizar);
	}
	
	private void setAulasMinimas() 
	{
		solucion.resetearAulas();
		solverAuxiliar = new SolverGoloso(instancia);
		solverAuxiliar.resolverSimple();
		lblAulasMnimasNº.setText(""+solverAuxiliar.getAulasAsignadas().size());
		solucion.resetearAulas();
	}
	
	private void setTextField() 
	{
		textFieldAulas = new JTextField();
		textFieldAulas.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldAulas.setBounds(314, 106, 110, 20);
		contentPanel.add(textFieldAulas);
		textFieldAulas.setColumns(10);
	}
	
	private void setButtons() 
	{
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!Graficador.esAulasValida(textFieldAulas.getText()))
					JOptionPane.showMessageDialog(null, "  Ingrese un número de aulas válido!\r\n");
				
				else if (Long.valueOf(textFieldAulas.getText()) < Integer.valueOf(lblAulasMnimasNº.getText()))
						JOptionPane.showMessageDialog(null, "Por defecto, el número de aulas ingresado no puede ser menor al mínimo sugerido. \r\n");
				else
				{
					setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					solucion.resetearAulas();
	        		procesoCostoso = new ProcesoAsignarAulasSW(instancia, 3, null, Integer.valueOf(textFieldAulas.getText()));
	        		procesoCostoso.execute();
	        		setpropertyChange(procesoCostoso);
				}
			}
		});
		btnAceptar.setIcon(new ImageIcon(VentanaGenetica.class.getResource("/Iconos/spell-7.png")));
		btnAceptar.setBounds(68, 167, 119, 23);
		contentPanel.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		btnCancelar.setIcon(new ImageIcon(VentanaGenetica.class.getResource("/Iconos/stop_2-7.png")));
		btnCancelar.setBounds(255, 167, 119, 23);
		contentPanel.add(btnCancelar);
	}
	
	protected void setpropertyChange(ProcesoAsignarAulasSW procesoCostoso) 
	{
		procesoCostoso.addPropertyChangeListener(new PropertyChangeListener() 
		{
			@Override
			public void propertyChange(PropertyChangeEvent evt) 
			{
				if ("state".equals(evt.getPropertyName())) 
				{
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) 
					{
						try 
						{
							solucion.setAulas(procesoCostoso.get());
							ventanaPadre.setInstancia(instancia);
							setCursor(null);
							ventanaPadre.cargarTabla();
	                		JOptionPane.showMessageDialog(null, "         Aulas asignadas!\r\n");
	                		dispose();
						} 
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		});
	}
}
