package Presentacion_VentanasListados;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import Logica_Materias.ListadoMaterias;
import Logica_Solver.Instancia;
import Logica_Solver.SolverColoreoGoloso;
import Logica_Solver.SolverGenetico;
import Logica_Solver.SolverGoloso;


public class ProcesoAsignarAulasSW extends SwingWorker<ArrayList<ListadoMaterias>, Integer>
{
	private Instancia instancia;
	private SolverGoloso solverSW;
	private SolverColoreoGoloso solverColorSW;
	private SolverGenetico solverGA;
	private JProgressBar barra;
	private int algoritmo;
	
	public ProcesoAsignarAulasSW (Instancia inst, int tipoAlgoritmo, 
									JProgressBar progressBar, int aulasDeseadas)
	{
		this.instancia = inst;
		this.barra = progressBar;
		this.algoritmo = tipoAlgoritmo;
		
		if(algoritmo == 0)
			this.solverSW = new SolverGoloso(instancia);
			
		else if (algoritmo == 1)
			this.solverColorSW = new SolverColoreoGoloso(instancia);
		
		else
			this.solverGA = new SolverGenetico(instancia, aulasDeseadas);
	}

	@Override
	protected ArrayList<ListadoMaterias> doInBackground() throws Exception 
	{
		if (algoritmo == 0)
		{
			this.solverSW.resolverSimple();
			generarEspera();
			return solverSW.getAulasAsignadas();
		}
		
		else if(algoritmo == 1)
		{
			this.solverColorSW.resolverConColor();
			generarEspera();
			return solverColorSW.getAulasAsignadas();
		}
			
		else
		{
			this.solverGA.resolverGenetico();
			return solverGA.getAulasAsignadas();
		}
	}
	
	private void generarEspera() 
	{
		Random random = new Random();
		
		for (int i = 0; i < 100; i++) 
		{	
			try 
			{
				Thread.sleep(random.nextInt(12));
			} 
			catch (InterruptedException e) 
			{
				String linea1 = ("    B�squeda interrumpida!\r\n");
				JOptionPane.showMessageDialog(null, linea1);
			}
			publish(i+1);
		}
	}

	@Override
	protected void process(List<Integer> chunks) 
	{
		if (barra != null)
			barra.setValue(chunks.get(0));
	}
	
	@Override
	protected void done() 
	{
		if (barra != null)
			barra.setValue(100);
	}
}
