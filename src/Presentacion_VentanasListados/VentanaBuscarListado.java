package Presentacion_VentanasListados;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

import Datos.Persistidor;
import Logica_Solver.Instancia;
import Presentacion_Main.Graficador;
import Presentacion_Main.MainForm;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class VentanaBuscarListado extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel titulo;
	private JLabel nombreLabel;
	private JTextField listadoAcargar;
	private JButton buttonCargar;
	private JButton btnExplorar;
	private JButton buttonCancelar;
	private Instancia instanciaPadre;
	private String listadoBuscado;
	private MainForm ventanaPadre;

	public VentanaBuscarListado(Instancia instancia, MainForm ventana) 
	{
		setResizable(false);
		setBounds(100, 100, 450, 240);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setLocationRelativeTo(null);
		setTitle("B�squeda de listado");
		
		this.instanciaPadre = instancia;
		listadoBuscado = "";
		ventanaPadre = ventana;
		
		setLabels();
		setTextField();
		setButtons();
	}
	
	private void setLabels() 
	{
		titulo = new JLabel("Indique  el nombre de listado a buscar y cargar:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		titulo.setBounds(58, 11, 328, 30);
		contentPanel.add(titulo);
		
		nombreLabel = new JLabel("Nombre del listado:");
		nombreLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		nombreLabel.setBounds(10, 95, 112, 14);
		contentPanel.add(nombreLabel);
	}
	
	private void setTextField() 
	{
		listadoAcargar = new JTextField();
		listadoAcargar.setHorizontalAlignment(SwingConstants.CENTER);
		listadoAcargar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		listadoAcargar.setBounds(219, 92, 215, 20);
		contentPanel.add(listadoAcargar);
		listadoAcargar.setColumns(10);
	}
	private void setButtons() 
	{
		buttonCargar = new JButton("Buscar");
		buttonCargar.setIcon(new ImageIcon(VentanaBuscarListado.class.getResource("/Iconos/search-7.png")));
		buttonCargar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonCargar.setBounds(10, 165, 120, 23);
		buttonCargar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (!Graficador.esNombreValido(listadoAcargar.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre correcto para el listado.\r\n");
				else
				{
					if (Persistidor.leerJSON("Archivos\\"+listadoAcargar.getText()+".JSON") == null)
						JOptionPane.showMessageDialog(null, "        Listado no encontrado! \r\n");
					
					else
					{
						instanciaPadre.setMaterias(Persistidor.leerJSON("Archivos\\"+listadoAcargar.getText()+".JSON"));
						JOptionPane.showMessageDialog(null, "        Listado encontrado! \r\n");
						listadoBuscado = listadoAcargar.getText();
						ventanaPadre.setTitle("Trabajo Pr�ctico N� 3: Al SUM de la biblioteca"+"      Listado cargado: "+listadoBuscado+".");
						dispose();
					}
				}	
			}
		});
		contentPanel.add(buttonCargar);
		
		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setIcon(new ImageIcon(VentanaBuscarListado.class.getResource("/Iconos/stop_2-7.png")));
		buttonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonCancelar.setBounds(314, 165, 120, 23);
		buttonCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(buttonCancelar);
		
		btnExplorar = new JButton("Explorar...");
		btnExplorar.setIcon(new ImageIcon(VentanaBuscarListado.class.getResource("/Iconos/browse_1-7.png")));
		btnExplorar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
			    
				if (returnValue == JFileChooser.APPROVE_OPTION) 
			    {
					try
					{
				        instanciaPadre.setMaterias(Persistidor.leerJSON(fileChooser.getSelectedFile().getAbsolutePath()));
						JOptionPane.showMessageDialog(null, "        Listado encontrado! \r\n");
				        listadoBuscado = (fileChooser.getSelectedFile().getName());
				        ventanaPadre.setTitle("Trabajo Pr�ctico N� 3: Al SUM de la biblioteca"+"      Listado cargado: "+listadoBuscado+".");
						dispose();
					}
					catch(Exception ex)
					{
						JOptionPane.showMessageDialog(null, "        Archivo incorrecto! \r\n");
					}
			        
			    }
			}
		});
		btnExplorar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnExplorar.setBounds(162, 166, 120, 23);
		contentPanel.add(btnExplorar);
	}
	
	public String getListadoBuscado()
	{
		return this.listadoBuscado;
	}
}
