package Presentacion_VentanasListados;

import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.SwingWorker;

import Logica_Solver.Instancia;
import Logica_Solver.Subconjunto;
import Presentacion_Main.MainForm;

import javax.swing.ImageIcon;
import javax.swing.JProgressBar;

public class VentanaElegirAlgoritmo extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lbltitulo;
	private JLabel lblAlgoritmoSimple;
	private JLabel lblAlgoritmocoloreoDe;
	private JLabel lblGa;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JRadioButton rdbtnAlgoSimple;
	private JRadioButton rdbtnColoreo;
	private JRadioButton radioButtonGA;
	private Instancia instancia;
	private Subconjunto solucion;
	private ProcesoAsignarAulasSW procesoCostoso;
	private MainForm ventanaPadre;
	private VentanaGenetica ventanaGA;
	private int tipoAlgoritmo;
	private JProgressBar progressBar;

	public VentanaElegirAlgoritmo(MainForm mainFormPadre) 
	{
		setResizable(false);
		setBounds(100, 100, 450, 330);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setLocationRelativeTo(null);
		setTitle("Elecci�n de Algoritmo");

		instancia = mainFormPadre.getInstancia();
		ventanaPadre = mainFormPadre;
		solucion = mainFormPadre.getSolucion();
		
		setLabels();
		setButtons();
		setRadio();
		setProgressBar();
	}

	private void setLabels() 
	{
		lbltitulo = new JLabel("Aqu\u00ED puede seleccionar el tipo de algoritmo deseado:");
		lbltitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbltitulo.setBounds(38, 11, 367, 23);
		contentPanel.add(lbltitulo);
		
		lblAlgoritmoSimple = new JLabel("Algoritmo \"Goloso de Asignaci\u00F3n Inmediata\":");
		lblAlgoritmoSimple.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAlgoritmoSimple.setBounds(10, 55, 256, 23);
		contentPanel.add(lblAlgoritmoSimple);
		
		lblAlgoritmocoloreoDe = new JLabel("Algoritmo \"Coloreo Goloso de Grafos \":");
		lblAlgoritmocoloreoDe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAlgoritmocoloreoDe.setBounds(10, 103, 228, 23);
		contentPanel.add(lblAlgoritmocoloreoDe);
		
		lblGa = new JLabel("Algoritmo Gen\u00E9tico-Evolutivo: \"Coloreo de Grafos:\"");
		lblGa.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblGa.setBounds(10, 146, 337, 23);
		contentPanel.add(lblGa);
	}

	private void setButtons() 
	{
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setIcon(new ImageIcon(VentanaElegirAlgoritmo.class.getResource("/Iconos/spell-7.png")));
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(71, 261, 115, 30);
		btnAceptar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (!radioButtonGA.isSelected() &&
					!rdbtnAlgoSimple.isSelected() &&
					!rdbtnColoreo.isSelected())
					
					JOptionPane.showMessageDialog(null, "         Selecione una opci�n!\r\n");
				
				else
				{
					if (radioButtonGA.isSelected())
					{
						ventanaGA = new VentanaGenetica(ventanaPadre, instancia);
						ventanaGA.setVisible(true);
						ventanaGA.setLocationRelativeTo(null);
						dispose();
					}
					else 
					{
						tipoAlgoritmo = 0;
						
						if(rdbtnColoreo.isSelected())
							tipoAlgoritmo = 1;
						
						setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						solucion.resetearAulas();
		        		procesoCostoso = new ProcesoAsignarAulasSW(instancia, tipoAlgoritmo, progressBar, 0);
		        		procesoCostoso.execute();
		        		setpropertyChange(procesoCostoso);
					}
				}
			}
		});
		contentPanel.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(VentanaElegirAlgoritmo.class.getResource("/Iconos/stop_2-7.png")));
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCancelar.setBounds(257, 261, 115, 30);
		btnCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(btnCancelar);
	}
	
	protected void setpropertyChange(ProcesoAsignarAulasSW procesoCostoso) 
	{
		procesoCostoso.addPropertyChangeListener(new PropertyChangeListener() 
		{
			@Override
			public void propertyChange(PropertyChangeEvent evt) 
			{
				if ("state".equals(evt.getPropertyName())) 
				{
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) 
					{
						try 
						{
							solucion.setAulas(procesoCostoso.get());
							ventanaPadre.setInstancia(instancia);
							setCursor(null);
							ventanaPadre.cargarTabla();
	                		JOptionPane.showMessageDialog(null, "         Aulas asignadas!\r\n");
	                		dispose();
						} 
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		});
	}
	
	private void setRadio() 
	{
		rdbtnAlgoSimple = new JRadioButton("");
		buttonGroup.add(rdbtnAlgoSimple);
		rdbtnAlgoSimple.setBounds(412, 55, 26, 23);
		contentPanel.add(rdbtnAlgoSimple);
		
		rdbtnColoreo = new JRadioButton("");
		buttonGroup.add(rdbtnColoreo);
		rdbtnColoreo.setBounds(412, 103, 26, 23);
		contentPanel.add(rdbtnColoreo);
		
		radioButtonGA = new JRadioButton("");
		buttonGroup.add(radioButtonGA);
		radioButtonGA.setBounds(412, 147, 26, 23);
		contentPanel.add(radioButtonGA);
	}
	
	private void setProgressBar() 
	{
		progressBar = new JProgressBar();
		progressBar.setBounds(10, 203, 424, 14);
		contentPanel.add(progressBar);
	}
}
