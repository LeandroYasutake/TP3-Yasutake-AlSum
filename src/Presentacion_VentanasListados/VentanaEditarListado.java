package Presentacion_VentanasListados;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Presentacion_Main.Graficador;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;

public class VentanaEditarListado extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private VentanaGuardarListado ventanaGuardarListado;
	private ListadoMaterias listadoEditable;
	private JLabel lblTitulo;
	private JLabel lblMateriaAremover;
	private JLabel materiaNombre;
	private JLabel horaInicioLabel;
	private JLabel horaFinLabel;
	private JLabel lblSeparador;
	private JLabel lblSeparador2;
	private JTextField nombreMateriaInput;
	private JTextField horaInicioInput;
	private JTextField horaFinInput;
	private JButton btnAgregarMateria;
	private JButton btnRemoverMateria;
	private JButton btnGuardarListado;
	private JButton btnCancelar;
	private JComboBox<Materia> comboBoxMateriaRemover;
	
	public VentanaEditarListado(ListadoMaterias listadoMaterias) 
	{
		setResizable(false);
		setBounds(100, 100, 520, 400);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		setTitle("Editor de listado de materias");
		contentPanel.setLayout(null);
		
		this.listadoEditable = listadoMaterias;
		
		setLabels();
		setButtons();
		setComboBox();
		setTextFields();
	}
	
	private void setLabels() 
	{
		lblTitulo = new JLabel("Aqu\u00ED puede editar el listado buscado:");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitulo.setBounds(128, 11, 258, 17);
		contentPanel.add(lblTitulo);
		
		lblMateriaAremover = new JLabel("Materia a remover:");
		lblMateriaAremover.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblMateriaAremover.setBounds(10, 63, 209, 14);
		contentPanel.add(lblMateriaAremover);
		
		materiaNombre = new JLabel("Materia nueva:");
		materiaNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		materiaNombre.setBounds(10, 145, 96, 14);
		contentPanel.add(materiaNombre);
		
		horaInicioLabel = new JLabel("Hora de inicio (24hs):");
		horaInicioLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaInicioLabel.setBounds(10, 185, 124, 14);
		contentPanel.add(horaInicioLabel);
		
		horaFinLabel = new JLabel("Hora de finalizaci\u00F3n (24hs):");
		horaFinLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaFinLabel.setBounds(10, 223, 164, 14);
		contentPanel.add(horaFinLabel);
		
		lblSeparador = new JLabel("------------------------------------------------------------");
		lblSeparador.setBounds(134, 102, 246, 14);
		contentPanel.add(lblSeparador);
		
		lblSeparador2 = new JLabel("------------------------------------------------------------");
		lblSeparador2.setBounds(131, 308, 252, 14);
		contentPanel.add(lblSeparador2);
	}
	
	private void setButtons() 
	{
		btnAgregarMateria = new JButton("Agregar materia");
		btnAgregarMateria.setIcon(new ImageIcon(VentanaEditarListado.class.getResource("/Iconos/add_folder-7.png")));
		btnAgregarMateria.setBounds(78, 263, 139, 23);
		btnAgregarMateria.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (!Graficador.esNombreValido(nombreMateriaInput.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre correcto para la materia deseada.\r\n");
				
				else if (!Graficador.esInicioValido(horaInicioInput.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese una hora de inicio correcta (24hs).\r\n");
				
				else if (!Graficador.esFinValido(horaFinInput.getText(), horaInicioInput.getText()))
					JOptionPane.showMessageDialog(null, "Por favor, ingrese una hora de finalización correcta (24hs).\r\n");
				
				else
				{
					Materia nuevaMateria = new Materia(nombreMateriaInput.getText(), Integer.parseInt(horaInicioInput.getText()), Integer.parseInt(horaFinInput.getText()));
					listadoEditable.getMaterias().add(nuevaMateria);
					JOptionPane.showMessageDialog(null, "Materia agregada exitosamente!\r\n");
					resetearComboBox();
					resetearLabels();
				}
			}
		});
		contentPanel.add(btnAgregarMateria);
		
		btnRemoverMateria = new JButton("Remover materia");
		btnRemoverMateria.setIcon(new ImageIcon(VentanaEditarListado.class.getResource("/Iconos/cut-7.png")));
		btnRemoverMateria.setBounds(295, 263, 139, 23);
		btnRemoverMateria.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (comboBoxMateriaRemover.getSelectedItem() == null)
					JOptionPane.showMessageDialog(null, "         Por favor, seleccione una materia\r\n");
				
				else
				{
					Materia materiaSeleccionada = (Materia) comboBoxMateriaRemover.getSelectedItem();
					listadoEditable.getMaterias().remove(materiaSeleccionada);
					resetearComboBox();
					JOptionPane.showMessageDialog(null, "         Materia removida del listado!\r\n");
				}
			}
		});
		contentPanel.add(btnRemoverMateria);
		
		btnGuardarListado = new JButton("Guardar listado");
		btnGuardarListado.setIcon(new ImageIcon(VentanaEditarListado.class.getResource("/Iconos/save-7.png")));
		btnGuardarListado.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ventanaGuardarListado = new VentanaGuardarListado(null, listadoEditable);
				ventanaGuardarListado.setVisible(true);
				ventanaGuardarListado.setLocationRelativeTo(null);
			}
		});
		btnGuardarListado.setBounds(78, 338, 139, 23);
		contentPanel.add(btnGuardarListado);

		btnCancelar = new JButton("Salir");
		btnCancelar.setIcon(new ImageIcon(VentanaEditarListado.class.getResource("/Iconos/stop_2-7.png")));
		btnCancelar.setBounds(295, 338, 139, 23);
		btnCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(btnCancelar);
	}
	
	private void resetearLabels() 
	{
		nombreMateriaInput.setText("");
		horaInicioInput.setText("");
		horaFinInput.setText("");
	}
	
	private void resetearComboBox()
	{
		comboBoxMateriaRemover.removeAllItems();
		Graficador.llenarComboBoxMateria(comboBoxMateriaRemover, listadoEditable.getMaterias());
		comboBoxMateriaRemover.setSelectedIndex(-1);
	}
	
	private void setComboBox() 
	{
		comboBoxMateriaRemover = new JComboBox<Materia>();
		comboBoxMateriaRemover.setBounds(264, 61, 240, 20);
		Graficador.llenarComboBoxMateria(comboBoxMateriaRemover, listadoEditable.getMaterias());
		comboBoxMateriaRemover.setSelectedIndex(-1);
		((JLabel)comboBoxMateriaRemover.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		contentPanel.add(comboBoxMateriaRemover);
	}
	
	private void setTextFields() 
	{
		nombreMateriaInput = new JTextField();
		nombreMateriaInput.setHorizontalAlignment(SwingConstants.CENTER);
		nombreMateriaInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		nombreMateriaInput.setBounds(212, 142, 292, 20);
		nombreMateriaInput.setColumns(10);
		contentPanel.add(nombreMateriaInput);
		
		horaInicioInput = new JTextField();
		horaInicioInput.setHorizontalAlignment(SwingConstants.CENTER);
		horaInicioInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaInicioInput.setBounds(212, 179, 292, 20);
		horaInicioInput.setColumns(10);
		contentPanel.add(horaInicioInput);
		
		horaFinInput = new JTextField();
		horaFinInput.setHorizontalAlignment(SwingConstants.CENTER);
		horaFinInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaFinInput.setBounds(212, 220, 292, 20);
		horaFinInput.setColumns(10);
		contentPanel.add(horaFinInput);
	}
}
