package Presentacion_VentanasListados;


import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import Logica_AG.Observador;
import Logica_AG.Poblacion;

import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
 
public class ObservadorVisual implements Observador 
{
	private JFrame frame;
	private Poblacion poblacion;	
	private static int iteracion=1;
	private XYSeriesCollection dataSet;
	private JFreeChart chart;
	private ChartPanel chartPanel;
	private JButton btnAceptar;

	public ObservadorVisual(Poblacion poblacion) 
	{	
		this.poblacion = poblacion;
		initialize();
	}
	private void initialize() 
	{
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 700, 550);
		frame.setTitle("Algoritmo Genetico. Coloreo de grafos.");
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
 
		setGrafico();
		setButtons();
	}
	
	private void setGrafico() 
	{
		setDatos();

		chart = ChartFactory.createXYLineChart
		(	
			"Población", 
			"Iteración",
			"Superposiciones ", 
			dataSet, 
			PlotOrientation.VERTICAL, 
			true, 
			false,
			false
		);
		chart.setBackgroundPaint(Color.LIGHT_GRAY);
		chartPanel = new ChartPanel(chart);
		chartPanel.setBounds(10, 10, 674, 449);
		frame.getContentPane().add(chartPanel);
	}
 
	private void setDatos()
	{
		dataSet = new XYSeriesCollection();
		dataSet.addSeries(new XYSeries("Promedio"));
		dataSet.addSeries(new XYSeries("Mejor"));
		dataSet.addSeries(new XYSeries("Peor"));
	}
	
	private void setButtons() 
	{
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setIcon(new ImageIcon(ObservadorVisual.class.getResource("/Iconos/spell-7.png")));
		btnAceptar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.setVisible(false);
			}
		});
		btnAceptar.setBounds(285, 482, 124, 29);
		frame.getContentPane().add(btnAceptar);
	}
 
	@Override
	public void notificar() 
	{
		dataSet.getSeries(0).add(iteracion, poblacion.fitnessPromedio());
		dataSet.getSeries(1).add(iteracion, poblacion.mejorIndividuo().fitness());
		dataSet.getSeries(2).add(iteracion, poblacion.peorIndividuo().fitness());	
		iteracion++;			
	}
}
