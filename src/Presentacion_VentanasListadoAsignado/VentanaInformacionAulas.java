package Presentacion_VentanasListadoAsignado;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import Logica_Materias.GestorAulas;
import Logica_Materias.ListadoMaterias;
import Logica_Solver.Subconjunto;
import Presentacion_Main.Graficador;

import javax.swing.ImageIcon;

public class VentanaInformacionAulas extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblListadoUtilizado;
	private JLabel lblnombreListado;
	private JLabel lblCantidadDeMaterias;
	private JLabel lblCantMat;
	private JLabel lblCantidadDeAulas;
	private JLabel N�AulasAsignadas;
	private JLabel lblAulasSubutilizadas;
	private JLabel N�aulasSub;
	private JLabel lblPromMatAulas;
	private JLabel N�PromMatAula;
	private JLabel lblPromHoraAulas;
	private JLabel N�PromHoraAula;
	private JButton btnGrficosDeResultado;
	private JButton btnEstadisticas;
	private JButton btnSalir;
	private ArrayList<ListadoMaterias> aulasAsignadas;
	private String listadoBuscado;
	private VentanaGraficosAulas ventanaGraficoAulas;
	private VentanaGraficosResolucion ventanaGraficosResolucion;

	public VentanaInformacionAulas(Subconjunto solucion, String listadoBuscado) 
	{
		setResizable(false);
		setBounds(100, 100, 510, 340);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		setTitle("Informaci�n de Aulas Asignadas");
		
		this.aulasAsignadas = solucion.getAulas();
		this.listadoBuscado = listadoBuscado;
		
		setLabels();
		setButton();
	}

	private void setLabels() 
	{
		contentPanel.setLayout(null);
		lblListadoUtilizado = new JLabel("Listado utilizado:");
		lblListadoUtilizado.setBounds(10, 11, 96, 14);
		lblListadoUtilizado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblListadoUtilizado);
		
		lblnombreListado = new JLabel(this.listadoBuscado);
		lblnombreListado.setBounds(317, 11, 177, 14);
		lblnombreListado.setHorizontalAlignment(SwingConstants.RIGHT);
		lblnombreListado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblnombreListado);
		
		lblCantidadDeMaterias = new JLabel("Cantidad de Materias:");
		lblCantidadDeMaterias.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCantidadDeMaterias.setBounds(10, 54, 132, 14);
		contentPanel.add(lblCantidadDeMaterias);
		
		lblCantMat = new JLabel(Graficador.getRedondeado(GestorAulas.getAulasSubutilizadasYpromedio(aulasAsignadas)[2]));
		lblCantMat.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCantMat.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCantMat.setBounds(448, 55, 46, 14);
		contentPanel.add(lblCantMat);
		
		lblCantidadDeAulas = new JLabel("Cantidad de aulas asignadas:");
		lblCantidadDeAulas.setBounds(10, 97, 177, 16);
		lblCantidadDeAulas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblCantidadDeAulas);
		
		N�AulasAsignadas = new JLabel(String.valueOf(this.aulasAsignadas.size()));
		N�AulasAsignadas.setBounds(317, 99, 177, 14);
		N�AulasAsignadas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		N�AulasAsignadas.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPanel.add(N�AulasAsignadas);
		
		lblAulasSubutilizadas = new JLabel("Aulas subutilizadas:");
		lblAulasSubutilizadas.setBounds(10, 142, 132, 21);
		lblAulasSubutilizadas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblAulasSubutilizadas);
		
		N�aulasSub = new JLabel(Graficador.getRedondeado(GestorAulas.getAulasSubutilizadasYpromedio(aulasAsignadas)[0]));
		N�aulasSub.setBounds(317, 149, 177, 14);
		N�aulasSub.setFont(new Font("Tahoma", Font.PLAIN, 13));
		N�aulasSub.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPanel.add(N�aulasSub);
		
		lblPromMatAulas = new JLabel("N\u00BA promedio materias/aulas:");
		lblPromMatAulas.setBounds(10, 192, 166, 14);
		lblPromMatAulas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblPromMatAulas);
		
		N�PromMatAula = new JLabel(Graficador.getRedondeado(GestorAulas.getPromedioMateriasAulas(aulasAsignadas)));
		N�PromMatAula.setBounds(317, 192, 177, 14);
		N�PromMatAula.setHorizontalAlignment(SwingConstants.RIGHT);
		N�PromMatAula.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(N�PromMatAula);
		
		lblPromHoraAulas = new JLabel("N\u00BA promedio horas/aulas:");
		lblPromHoraAulas.setBounds(10, 235, 166, 14);
		lblPromHoraAulas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblPromHoraAulas);
		
		N�PromHoraAula = new JLabel(Graficador.getRedondeado(GestorAulas.getAulasSubutilizadasYpromedio(aulasAsignadas)[1]));
		N�PromHoraAula.setBounds(317, 235, 177, 14);
		N�PromHoraAula.setFont(new Font("Tahoma", Font.PLAIN, 13));
		N�PromHoraAula.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPanel.add(N�PromHoraAula);
	}
	
	private void setButton() 
	{
		btnGrficosDeResultado = new JButton("Gr\u00E1ficos del resultado");
		btnGrficosDeResultado.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				ventanaGraficosResolucion = new VentanaGraficosResolucion
				(
					listadoBuscado,
					aulasAsignadas.size(),
					GestorAulas.getAulasSubutilizadasYpromedio(aulasAsignadas)[2],
					GestorAulas.getAulasSubutilizadasYpromedio(aulasAsignadas)[0],
					GestorAulas.getPromedioMateriasAulas(aulasAsignadas),
					GestorAulas.getAulasSubutilizadasYpromedio(aulasAsignadas)[1]
				);
				ventanaGraficosResolucion.setVisible(true);
				ventanaGraficosResolucion.setLocationRelativeTo(null);
				dispose();
			}
		});
		btnGrficosDeResultado.setIcon(new ImageIcon(VentanaInformacionAulas.class.getResource("/javax/swing/plaf/metal/icons/ocean/computer.gif")));
		btnGrficosDeResultado.setBounds(10, 279, 166, 23);
		contentPanel.add(btnGrficosDeResultado);
		
		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(VentanaInformacionAulas.class.getResource("/Iconos/stop_2-7.png")));
		btnSalir.setBounds(208, 278, 96, 23);
		btnSalir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(btnSalir);
		
		btnEstadisticas = new JButton("Gr\u00E1ficos de aulas");
		btnEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				ventanaGraficoAulas = new VentanaGraficosAulas(aulasAsignadas);
				ventanaGraficoAulas.setVisible(true);
				ventanaGraficoAulas.setLocationRelativeTo(null);
				dispose();
			}
		});
		btnEstadisticas.setIcon(new ImageIcon(VentanaInformacionAulas.class.getResource("/javax/swing/plaf/metal/icons/ocean/computer.gif")));
		btnEstadisticas.setBounds(337, 279, 157, 23);
		contentPanel.add(btnEstadisticas);
	}
}
