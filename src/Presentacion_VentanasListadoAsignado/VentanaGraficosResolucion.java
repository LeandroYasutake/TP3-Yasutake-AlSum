package Presentacion_VentanasListadoAsignado;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import javax.swing.ImageIcon;

import org.jfree.chart.ChartPanel;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaGraficosResolucion extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton btnSalir;
	private GraficoBarra grafico;
	private ChartPanel chartPanel;
	private DefaultCategoryDataset dataset;
	private String listado;
	private double cantMaterias;
	private double aulasSub;
	private double promedioMatAula;
	private double promedioHoraAula;
	private int cantAulas;

	public VentanaGraficosResolucion(String listadoBuscado, int n�Aulas, double cantMaterias, double aulasSub, double promedioMatAula, double promedioHoraAula) 
	{
		setResizable(false);
		setBounds(100, 100, 650, 570);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		setTitle("Gr�fico de la Resoluci�n");
		contentPanel.setLayout(null);
		
		this.listado = listadoBuscado;
		this.cantMaterias = cantMaterias;
		this.aulasSub = aulasSub;
		this.promedioMatAula = promedioMatAula;
		this.promedioHoraAula = promedioHoraAula;
		this.cantAulas = n�Aulas;
		
		setButtons();
		setDatos();
		setGrafico();
	}
	
	private void setButtons() 
	{
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
		});
		btnSalir.setIcon(new ImageIcon(VentanaGraficosResolucion.class.getResource("/Iconos/stop_2-7.png")));
		btnSalir.setBounds(256, 499, 131, 32);
		contentPanel.add(btnSalir);
	}
	
	private void setDatos() 
	{
		dataset = new DefaultCategoryDataset();
		dataset.setValue(Double.valueOf(cantAulas), "Cantidad de Aulas", "");
		dataset.setValue(Double.valueOf(cantMaterias), "Cantida de Materias", "");
		dataset.setValue(Double.valueOf(aulasSub), "Aulas Subutilizadas", "");
		dataset.setValue(Double.valueOf(promedioMatAula), "Promedio Materias/Aulas", "");
		dataset.setValue(Double.valueOf(promedioHoraAula), "Promedio Horas/Aula", "");
	}
	
	private void setGrafico() 
	{
		grafico = new GraficoBarra("Estad�sticas de la Resoluci�n: Listado "+listado, dataset);
		chartPanel = new ChartPanel(grafico.getChart());
		chartPanel.setBounds(25, 11, 600, 477);
		contentPanel.add(chartPanel);
	}
}
