package Presentacion_VentanasListadoAsignado;
import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JButton;

import Logica_Materias.GestorAulas;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Subconjunto;
import Presentacion_Main.ComboItem;
import Presentacion_Main.Graficador;
import Presentacion_Main.MainForm;

import javax.swing.ImageIcon;

public class VentanaCambiosManuales extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private MainForm ventanaPadre;
	private VentanaEditarAulas ventanaEditarAulas;
	private JLabel lblTitulo;
	private JLabel lblAulaOrigen;
	private JLabel lblMaterias;
	private JLabel lblAulaDestino;
	private JComboBox<ComboItem> comboBoxAulaOrigen;
	private JComboBox<Materia> comboBoxMaterias;
	private JComboBox<ComboItem> comboBoxAulaDestino;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JButton btnOtrasOpciones;
	private Materia materiaSeleccionada;
	private ListadoMaterias aulaAuxOrigen;
	private ListadoMaterias aulaAuxDestino;
	private Subconjunto solucion;

	public VentanaCambiosManuales(MainForm mainFormPadre) 
	{
		setResizable(false);
		setBounds(100, 100, 450, 274);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		setTitle("Intercambio de materias");
		contentPanel.setLayout(null);
		
		ventanaPadre = mainFormPadre;
		solucion = mainFormPadre.getSolucion();
		
		setLabels();
		setComboBox();
		setButtons(mainFormPadre);
	}
	
	private void setLabels() 
	{
		lblTitulo = new JLabel("Aqu\u00ED puede realizar el intercambio de materias entre aulas:");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitulo.setBounds(20, 25, 404, 20);
		contentPanel.add(lblTitulo);
		
		lblAulaOrigen = new JLabel("Aula origen:");
		lblAulaOrigen.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAulaOrigen.setBounds(10, 78, 70, 20);
		contentPanel.add(lblAulaOrigen);
		
		lblMaterias = new JLabel("Materia:");
		lblMaterias.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblMaterias.setBounds(10, 125, 56, 14);
		contentPanel.add(lblMaterias);
		
		lblAulaDestino = new JLabel("Aula destino:");
		lblAulaDestino.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAulaDestino.setBounds(10, 166, 81, 14);
		contentPanel.add(lblAulaDestino);
	}

	private void setComboBox() 
	{
		comboBoxAulaOrigen = new JComboBox<ComboItem>();
		comboBoxAulaOrigen.setFont(new Font("Tahoma", Font.PLAIN, 13));
		comboBoxAulaOrigen.setBounds(197, 78, 237, 20);
		((JLabel)comboBoxAulaOrigen.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		Graficador.llenarComboBoxAula(comboBoxAulaOrigen, this.solucion.getAulas());
		comboBoxAulaOrigen.setSelectedIndex(-1);
		comboBoxAulaOrigen.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				comboBoxMaterias.removeAllItems();
				ComboItem item = (ComboItem) comboBoxAulaOrigen.getSelectedItem();
				if (item != null)
				{
					ListadoMaterias materias = item.getValue();
					Graficador.llenarComboBoxMateria(comboBoxMaterias, materias.getMaterias());
				}
				comboBoxMaterias.setSelectedIndex(-1);
			}
		});
		contentPanel.add(comboBoxAulaOrigen);
		
		comboBoxMaterias = new JComboBox<Materia>();
		comboBoxMaterias.setFont(new Font("Tahoma", Font.PLAIN, 13));
		comboBoxMaterias.setBounds(197, 119, 237, 20);
		comboBoxMaterias.setSelectedIndex(-1);
		((JLabel)comboBoxMaterias.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		contentPanel.add(comboBoxMaterias);
		
		comboBoxAulaDestino = new JComboBox<ComboItem>();
		comboBoxAulaDestino.setFont(new Font("Tahoma", Font.PLAIN, 13));
		comboBoxAulaDestino.setBounds(197, 160, 237, 20);
		((JLabel)comboBoxAulaDestino.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		Graficador.llenarComboBoxAula(comboBoxAulaDestino, this.solucion.getAulas());
		comboBoxAulaDestino.setSelectedIndex(-1);
		contentPanel.add(comboBoxAulaDestino);
	}
	
	private void setButtons(MainForm mainFormPadre) 
	{
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setIcon(new ImageIcon(VentanaCambiosManuales.class.getResource("/Iconos/spell-7.png")));
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(10, 211, 106, 25);
		btnAceptar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (comboBoxMaterias.getSelectedItem() == null || 
						comboBoxAulaOrigen.getSelectedItem() == null || 
						comboBoxAulaDestino.getSelectedItem() == null)
					JOptionPane.showMessageDialog(null, "         Por favor, seleccione un �tem correcto\r\n");
				
				else
				{
					materiaSeleccionada= (Materia) comboBoxMaterias.getSelectedItem();
					
					ComboItem item1 = (ComboItem) comboBoxAulaOrigen.getSelectedItem();
					aulaAuxOrigen = (ListadoMaterias) item1.getValue();
					
					ComboItem item2 = (ComboItem) comboBoxAulaDestino.getSelectedItem();
					aulaAuxDestino = (ListadoMaterias) item2.getValue();
					
					if (GestorAulas.intercambiarMaterias(materiaSeleccionada, aulaAuxOrigen, aulaAuxDestino))
					{
						JOptionPane.showMessageDialog(null, "         Intercambio exisoto!\r\n");
						ventanaPadre.cargarTabla();
						updateComboBoxs();
					}
					
					else
						JOptionPane.showMessageDialog(null, "         Conflicto de horarios!\r\n");
				}
			}
		});
		contentPanel.add(btnAceptar);
		
		btnOtrasOpciones = new JButton("Agregar/Quitar...");
		btnOtrasOpciones.setIcon(new ImageIcon(VentanaCambiosManuales.class.getResource("/Iconos/themes_2-7.png")));
		btnOtrasOpciones.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnOtrasOpciones.setBounds(144, 212, 156, 25);
		btnOtrasOpciones.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				ventanaEditarAulas = new VentanaEditarAulas(ventanaPadre);
				ventanaEditarAulas.setVisible(true);
				ventanaEditarAulas.setLocationRelativeTo(null);
				dispose();
			}
		});
		contentPanel.add(btnOtrasOpciones);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(VentanaCambiosManuales.class.getResource("/Iconos/stop_2-7.png")));
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCancelar.setBounds(328, 211, 106, 25);
		btnCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(btnCancelar);
	}
	
	protected void updateComboBoxs() 
	{
		comboBoxAulaOrigen.removeAllItems();
		Graficador.llenarComboBoxAula(comboBoxAulaOrigen, this.solucion.getAulas());
		comboBoxAulaOrigen.setSelectedIndex(-1);
		
		comboBoxMaterias.removeAllItems();
		
		comboBoxAulaDestino.removeAllItems();
		Graficador.llenarComboBoxAula(comboBoxAulaDestino, this.solucion.getAulas());
		comboBoxAulaDestino.setSelectedIndex(-1);
	}
}
