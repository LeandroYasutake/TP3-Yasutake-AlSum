package Presentacion_VentanasListadoAsignado;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

public class GraficoTorta
{
	private ChartPanel chartPanel;
	private JFreeChart chart;
	private PiePlot3D plot;

    public GraficoTorta(String titulo, DefaultPieDataset datos) 
    {
        chart = ChartFactory.createPieChart3D
        (
        	titulo,  
        	datos,                   
            true,                  
            true,
            false
        );
        chart.setBackgroundPaint(Color.LIGHT_GRAY);
        chartPanel = new ChartPanel(chart);
        
        setPlot();
        Rotator rotator = new Rotator(plot);
        
        rotator.start();
    }
    
    private void setPlot() 
    {
    	plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(270);
        plot.setDirection(Rotation.ANTICLOCKWISE);
        plot.setForegroundAlpha(0.80f);
        plot.setInteriorGap(0.125);
	}

	public ChartPanel getChart()
    {
    	return chartPanel;
    }
}

class Rotator extends Timer implements ActionListener 
{
	private static final long serialVersionUID = 1L;
    private PiePlot3D plot;
    private int angle = 270;

    Rotator(PiePlot3D plot) 
    {
        super(100, null);
        this.plot = plot;
        addActionListener(this);
    }
    
    public void actionPerformed(final ActionEvent event) 
    {
        this.plot.setStartAngle(angle);
        this.angle = this.angle + 1;
        
        if (this.angle == 360) 
            this.angle = 0;
    }

}