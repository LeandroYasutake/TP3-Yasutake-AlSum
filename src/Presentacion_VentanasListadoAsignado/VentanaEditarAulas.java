package Presentacion_VentanasListadoAsignado;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Logica_Solver.Subconjunto;
import Presentacion_Main.ComboItem;
import Presentacion_Main.Graficador;
import Presentacion_Main.MainForm;

import javax.swing.JButton;
import javax.swing.ImageIcon;

public class VentanaEditarAulas extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private Subconjunto solucion;
	private MainForm ventanaPadre;
	private JComboBox<ComboItem> comboBoxAulas;
	private JComboBox<Materia> comboBoxMaterias;
	private JLabel lblMateriaARemover;
	private JLabel lblTitulo;
	private JLabel lblAulaAEditar;
	private JLabel horaFinLabel;
	private JLabel materiaNombre;
	private JLabel horaInicioLabel;
	private JLabel lblSeparador;
	private JLabel lblSeparador2;
	private JButton btnCancelar;
	private JButton btnRemoverMateria;
	private JButton btnAddAula;
	private Materia materiaSeleccionada;
	private ListadoMaterias aulaAeditar;
	private JTextField nombreMateriaInput;
	private JTextField horaInicioInput;
	private JTextField horaFinInput;
	private JButton btnAgregarMateria;
	private JButton btnEliminarAula;
	private JButton btnEliminarVacias;

	public VentanaEditarAulas(MainForm ventana) 
	{
		setResizable(false);
		setBounds(100, 100, 510, 450);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setTitle("Editor de Aulas");
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		this.solucion = ventana.getSolucion();
		this.ventanaPadre = ventana;
		
		setLabels();
		setButtons();
		setComboBoxs();
		setTextFields();
	}

	private void setLabels() 
	{
		lblTitulo = new JLabel("Aqu\u00ED puede editar las aulas asignadas:");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitulo.setBounds(122, 11, 260, 17);
		contentPanel.add(lblTitulo);
		
		lblAulaAEditar = new JLabel("Aula a editar:");
		lblAulaAEditar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAulaAEditar.setBounds(10, 55, 85, 17);
		contentPanel.add(lblAulaAEditar);
		
		lblMateriaARemover = new JLabel("Materia a remover:");
		lblMateriaARemover.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblMateriaARemover.setBounds(10, 132, 111, 14);
		contentPanel.add(lblMateriaARemover);
		
		lblSeparador = new JLabel("-----------------------------------------");
		lblSeparador.setBounds(170, 178, 164, 14);
		contentPanel.add(lblSeparador);
		
		lblSeparador2 = new JLabel("-----------------------------------------");
		lblSeparador2.setBounds(170, 85, 164, 14);
		contentPanel.add(lblSeparador2);
		
		materiaNombre = new JLabel("Materia nueva:");
		materiaNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		materiaNombre.setBounds(10, 224, 96, 14);
		contentPanel.add(materiaNombre);
		
		horaInicioLabel = new JLabel("Hora de inicio (24hs):");
		horaInicioLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaInicioLabel.setBounds(10, 264, 124, 14);
		contentPanel.add(horaInicioLabel);
		
		horaFinLabel = new JLabel("Hora de finalizaci\u00F3n (24hs):");
		horaFinLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaFinLabel.setBounds(10, 302, 164, 14);
		contentPanel.add(horaFinLabel);
	}
	
	private void setButtons() 
	{
		btnAgregarMateria = new JButton("Agregar materia");
		btnAgregarMateria.setIcon(new ImageIcon(VentanaEditarAulas.class.getResource("/Iconos/add_folder-7.png")));
		btnAgregarMateria.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (comboBoxAulas.getSelectedItem() == null)
						JOptionPane.showMessageDialog(null, "Por favor, seleccione un aula destino para la materia nueva.\r\n");
				
				else
				{
					ComboItem item = (ComboItem) comboBoxAulas.getSelectedItem();
					aulaAeditar = item.getValue();
					
					if (!Graficador.esNombreValido(nombreMateriaInput.getText()))
						JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre correcto para la materia deseada.\r\n");
					
					else if (!Graficador.esInicioValido(horaInicioInput.getText()))
						JOptionPane.showMessageDialog(null, "Por favor, ingrese una hora de inicio correcta (24hs).\r\n");
					
					else if (!Graficador.esFinValido(horaFinInput.getText(), horaInicioInput.getText()))
						JOptionPane.showMessageDialog(null, "Por favor, ingrese una hora de finalizaci�n correcta (24hs).\r\n");
					
					else
					{
						Materia nuevaMateria = new Materia(nombreMateriaInput.getText(), Integer.parseInt(horaInicioInput.getText()), Integer.parseInt(horaFinInput.getText()));
						
						if (aulaAeditar.hayOcupacion(nuevaMateria.getHoraInicio(), nuevaMateria.getHoraFin()))
							JOptionPane.showMessageDialog(null, "Conflicto de horarios!\r\n");
						
						else
						{
							aulaAeditar.getMaterias().add(nuevaMateria);
							JOptionPane.showMessageDialog(null, "Materia agregada exitosamente!\r\n");
							resetearLabels();
							updateComboBoxs();
							ventanaPadre.cargarTabla();
						}
					}
				}
			}
		});
		btnAgregarMateria.setBounds(10, 343, 154, 23);
		contentPanel.add(btnAgregarMateria);
		
		btnRemoverMateria = new JButton("Remover materia");
		btnRemoverMateria.setIcon(new ImageIcon(VentanaEditarAulas.class.getResource("/Iconos/cut-7.png")));
		btnRemoverMateria.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (comboBoxAulas.getSelectedItem() == null || 
					comboBoxMaterias.getSelectedItem() == null)
					JOptionPane.showMessageDialog(null, "Por favor, seleccione un �tem correcto para la remoci�n.\r\n");
				
				else
				{
					ComboItem item = (ComboItem) comboBoxAulas.getSelectedItem();
					aulaAeditar = item.getValue();
					materiaSeleccionada = (Materia) comboBoxMaterias.getSelectedItem();
					aulaAeditar.removeMateria(materiaSeleccionada);
					ventanaPadre.cargarTabla();
					updateComboBoxs();
					JOptionPane.showMessageDialog(null, "        Materia removida del aula!\r\n");
				}
			}
		});
		btnRemoverMateria.setBounds(174, 343, 154, 23);
		contentPanel.add(btnRemoverMateria);
		
		btnAddAula = new JButton("Agregar aula");
		btnAddAula.setIcon(new ImageIcon(VentanaEditarAulas.class.getResource("/Iconos/add_folder-7.png")));
		btnAddAula.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				aulaAeditar = new ListadoMaterias();
				solucion.getAulas().add(aulaAeditar);
				ventanaPadre.cargarTabla();
				updateComboBoxs();
				JOptionPane.showMessageDialog(null, "        Aula nueva agregada!\r\n");
			}
		});
		btnAddAula.setBounds(338, 343, 154, 23);
		contentPanel.add(btnAddAula);
		
		btnEliminarAula = new JButton("Eliminar aula");
		btnEliminarAula.setIcon(new ImageIcon(VentanaEditarAulas.class.getResource("/Iconos/delete-8.png")));
		btnEliminarAula.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (comboBoxAulas.getSelectedItem() == null)
					JOptionPane.showMessageDialog(null, "Por favor, seleccione un aula para su remoci�n.\r\n");
				else
				{
					ComboItem item = (ComboItem) comboBoxAulas.getSelectedItem();
					aulaAeditar = item.getValue();
					solucion.getAulas().remove(aulaAeditar);
					ventanaPadre.cargarTabla();
					updateComboBoxs();
					JOptionPane.showMessageDialog(null, "           Aula removida!\r\n");
				}
			}
		});
		btnEliminarAula.setBounds(10, 388, 154, 23);
		contentPanel.add(btnEliminarAula);
		
		btnEliminarVacias = new JButton("Eliminar aulas vac\u00EDas");
		btnEliminarVacias.setIcon(new ImageIcon(VentanaEditarAulas.class.getResource("/Iconos/delete-8.png")));
		btnEliminarVacias.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				solucion.eliminarAulasVacias();
				ventanaPadre.cargarTabla();
				updateComboBoxs();
			}
		});
		btnEliminarVacias.setBounds(174, 388, 154, 23);
		contentPanel.add(btnEliminarVacias);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(VentanaEditarAulas.class.getResource("/Iconos/stop_2-7.png")));
		btnCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		btnCancelar.setBounds(338, 388, 154, 23);
		contentPanel.add(btnCancelar);
		
	}
	
	private void resetearLabels() 
	{
		nombreMateriaInput.setText("");
		horaInicioInput.setText("");
		horaFinInput.setText("");
	}
	
	protected void updateComboBoxs() 
	{
		comboBoxAulas.removeAllItems();
		comboBoxMaterias.removeAllItems();
		Graficador.llenarComboBoxAula(comboBoxAulas, this.solucion.getAulas());
		comboBoxAulas.setSelectedIndex(-1);
	}

	private void setComboBoxs() 
	{
		comboBoxAulas = new JComboBox<ComboItem>();
		comboBoxAulas.setBounds(275, 54, 219, 20);
		Graficador.llenarComboBoxAula(comboBoxAulas, this.solucion.getAulas());
		comboBoxAulas.setSelectedIndex(-1);
		((JLabel)comboBoxAulas.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		comboBoxAulas.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				comboBoxMaterias.removeAllItems();
				ComboItem item = (ComboItem) comboBoxAulas.getSelectedItem();
				if (item != null)
				{
					ListadoMaterias materias = item.getValue();
					Graficador.llenarComboBoxMateria(comboBoxMaterias, materias.getMaterias());
				}
				comboBoxMaterias.setSelectedIndex(-1);
			}
		});
		contentPanel.add(comboBoxAulas);
		
		comboBoxMaterias = new JComboBox<Materia>();
		comboBoxMaterias.setBounds(275, 130, 219, 20);
		comboBoxMaterias.setSelectedIndex(-1);
		((JLabel)comboBoxMaterias.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		contentPanel.add(comboBoxMaterias);
	}
	
	private void setTextFields() 
	{
		nombreMateriaInput = new JTextField();
		nombreMateriaInput.setHorizontalAlignment(SwingConstants.CENTER);
		nombreMateriaInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		nombreMateriaInput.setBounds(228, 218, 266, 20);
		nombreMateriaInput.setColumns(10);
		contentPanel.add(nombreMateriaInput);
		
		horaInicioInput = new JTextField();
		horaInicioInput.setHorizontalAlignment(SwingConstants.CENTER);
		horaInicioInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaInicioInput.setBounds(228, 255, 266, 20);
		horaInicioInput.setColumns(10);
		contentPanel.add(horaInicioInput);
		
		horaFinInput = new JTextField();
		horaFinInput.setHorizontalAlignment(SwingConstants.CENTER);
		horaFinInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		horaFinInput.setBounds(228, 296, 266, 20);
		horaFinInput.setColumns(10);
		contentPanel.add(horaFinInput);
	}
}
