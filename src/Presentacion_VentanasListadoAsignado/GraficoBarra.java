package Presentacion_VentanasListadoAsignado;

import java.awt.Color;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;

public class GraficoBarra 
{
	private JFreeChart chart;
	private CategoryPlot plot;
	private CategoryAxis axis;
	
	public GraficoBarra(String titulo, CategoryDataset datos) 
	{
		chart = ChartFactory.createBarChart3D
		(
			titulo, 
			"Datos", 
			"Valores", 
			datos, 
			PlotOrientation.VERTICAL, 
			true, 
			true, 
			false 
		);
		chart.setBackgroundPaint(Color.LIGHT_GRAY);
		plot = chart.getCategoryPlot();
		axis = plot.getDomainAxis();
		axis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0));
	}
	
	public JFreeChart getChart()
	{
		return this.chart;
	}
}