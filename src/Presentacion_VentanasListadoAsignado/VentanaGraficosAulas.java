package Presentacion_VentanasListadoAsignado;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logica_Materias.GestorAulas;
import Logica_Materias.ListadoMaterias;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;

import org.jfree.data.general.DefaultPieDataset;

import Presentacion_Main.ComboItem;
import Presentacion_Main.Graficador;

import javax.swing.JButton;
import javax.swing.ImageIcon;

public class VentanaGraficosAulas extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lbltitulo;
	private JLabel lblAula;;
	private JComboBox<ComboItem> comboBoxAulas;
	private ArrayList<ListadoMaterias> aulas;
	private GraficoTorta grafico;
	private DefaultPieDataset datos;
	private JButton btnMostrar;
	private JButton btnSalir;
	
	public VentanaGraficosAulas(ArrayList<ListadoMaterias> aulasAsignadas) 
	{
		setResizable(false);
		setBounds(100, 100, 650, 570);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setTitle("Gr�fico de Aulas");
		setLocationRelativeTo(null);
		
		this.aulas = aulasAsignadas;
		
		setLabels();
		setButtons();
		setComboBox();
	}
	private void setLabels() 
	{
		lbltitulo = new JLabel("Seleccione un aula para la visualizaci\u00F3n:");
		lbltitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbltitulo.setBounds(188, 11, 268, 20);
		contentPanel.add(lbltitulo);
		
		lblAula = new JLabel("Aula:");
		lblAula.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAula.setBounds(10, 61, 46, 14);
		contentPanel.add(lblAula);
	}
	
	private void setButtons() 
	{
		btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				contentPanel.removeAll();
				resetPanel();
				if (comboBoxAulas.getSelectedItem() == null)
					JOptionPane.showMessageDialog(null, "         Por favor, seleccione un aula!\r\n");
				else
				{
					ComboItem item = (ComboItem) comboBoxAulas.getSelectedItem();
					ListadoMaterias aula = item.getValue();
					setearDatos(aula);
					setearGrafico();
				}
			}
		});
		btnMostrar.setIcon(new ImageIcon(VentanaGraficosAulas.class.getResource("/Iconos/spell-7.png")));
		btnMostrar.setBounds(135, 494, 119, 37);
		contentPanel.add(btnMostrar);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
		});
		btnSalir.setIcon(new ImageIcon(VentanaGraficosAulas.class.getResource("/Iconos/stop_2-7.png")));
		btnSalir.setBounds(389, 494, 119, 37);
		contentPanel.add(btnSalir);
	}
	
	protected void setearDatos(ListadoMaterias aula) 
	{
		datos = new DefaultPieDataset();
		
		double horasVacias = GestorAulas.getInfoAula(aula)[1];
		int stringHorasVacias = (int) horasVacias;
		String porcentajeHorasVacias = Graficador.getRedondeado(horasVacias*100/14);
		
		double horasUtilizadas = GestorAulas.getInfoAula(aula)[0];
		int stringHorasUtilizadas = (int) horasUtilizadas;
		String porcentajeHorasUtilizadas = Graficador.getRedondeado(horasUtilizadas*100/14);
		
		datos.setValue("Horas Vac�as \r\n"+stringHorasVacias+" hs.\r\n%"+porcentajeHorasVacias, horasVacias);
		datos.setValue("Horas Utilizadas \r\n"+stringHorasUtilizadas+" hs.\r\n%"+porcentajeHorasUtilizadas, horasUtilizadas);
	}
	
	protected void setearGrafico() 
	{
		grafico = new GraficoTorta("Estad�sticas del Aula", datos);
		grafico.getChart().setBounds(25, 100, 600, 375);
		contentPanel.add(grafico.getChart());
	}
	
	private void setComboBox() 
	{
		comboBoxAulas = new JComboBox<ComboItem>();
		comboBoxAulas.setBounds(426, 59, 208, 20);
		comboBoxAulas.setSelectedItem(-1);
		Graficador.llenarComboBoxAula(comboBoxAulas, aulas);
		((JLabel)comboBoxAulas.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		contentPanel.add(comboBoxAulas);
		
		ComboItem item = (ComboItem) comboBoxAulas.getSelectedItem();
		ListadoMaterias aula = item.getValue();
		setearDatos(aula);
		setearGrafico();
	}
	
	protected void resetPanel() 
	{
		contentPanel.add(lbltitulo);
		contentPanel.add(lblAula);
		contentPanel.add(btnMostrar);
		contentPanel.add(btnSalir);
		contentPanel.add(comboBoxAulas);
	}
}
