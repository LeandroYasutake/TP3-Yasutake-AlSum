package Logica_Solver;

import java.util.ArrayList;
import java.util.HashSet;

import Logica_AG.GeneradorRandom;
import Logica_AG.Individuo;
import Logica_AG.Poblacion;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;
import Presentacion_VentanasListados.ObservadorVisual;

public class SolverGenetico 
{
	private Poblacion poblacion;
	private Instancia instancia;
	private Individuo mejorIndividuo;
	private ObservadorVisual observador;
	private Subconjunto solucion;
	
	public SolverGenetico(Instancia inst, int aulasDeseadas)
	{
		setGeneradores();
		this.solucion = new Subconjunto();
		this.instancia = inst;
		this.poblacion = new Poblacion(instancia, aulasDeseadas);
		this.mejorIndividuo = null;
		this.observador = new ObservadorVisual(poblacion);
	}

	private void setGeneradores() 
	{
		Individuo.setGenerador(new GeneradorRandom());
		Poblacion.setGenerador(new GeneradorRandom());
	}
	
	public void resolverGenetico()
	{
		poblacion.registrar(observador);
		mejorIndividuo = poblacion.simular();
		asignarAulasColoreadas2(mejorIndividuo.getColoresAsignados());
	}
	
	public void asignarAulasColoreadas2(int[] coloresAsignados)
	{
		HashSet<Integer> coloresSinRepetir = new HashSet<Integer>();
		
		SolverColoreoGoloso.getColoresUnicos(coloresAsignados, coloresSinRepetir);
		SolverColoreoGoloso.getAulasSinRepetir(coloresSinRepetir, solucion);
		simplificarColores(coloresSinRepetir, coloresAsignados);
		
		for (int materia = 0 ; materia < coloresAsignados.length ; materia++)
		{
			ListadoMaterias aulaDestino = solucion.getAulas().get(coloresAsignados[materia]);
			Materia materiaAsignable = instancia.getMaterias().get(materia);
			
			aulaDestino.addMateria(materiaAsignable);
		}
	}
	
	//dado que los colores resultantes pueden ser variados segun la entrada del problema,
	//los transformo en enteros mas simples. 
	private void simplificarColores(HashSet<Integer> coloresSinRepetir, int[] coloresAsignados)
	{
		int colorSimplificado = 0;
		
		for (int color : coloresSinRepetir)
		{
			for (int color2 = 0 ; color2 < coloresAsignados.length ; color2++)
				if (color == coloresAsignados[color2])
					coloresAsignados[color2] = colorSimplificado;
			
			colorSimplificado++;
		}
	}

	public ArrayList<ListadoMaterias> getAulasAsignadas() 
	{
		return solucion.getAulas();
	}
}
