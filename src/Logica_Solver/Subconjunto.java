package Logica_Solver;

import java.util.ArrayList;
import java.util.Iterator;

import Logica_Materias.ListadoMaterias;

public class Subconjunto 
{
	private ArrayList<ListadoMaterias> aulas;
	
	public Subconjunto()
	{
		this.aulas = new ArrayList<ListadoMaterias>();
	}
	
	public ArrayList<ListadoMaterias> getAulas() 
	{
		return this.aulas;
	}
	
	public void setAulas(ArrayList<ListadoMaterias> aulas) 
	{
		this.aulas = aulas;
	}
	
	public void resetearAulas()
	{
		this.aulas = new ArrayList<ListadoMaterias>();
	}
	
	public void eliminarAulasVacias()
	{
		Iterator<ListadoMaterias> ite = aulas.iterator();
    	
    	while(ite.hasNext()) 
    	{
    		ListadoMaterias aula = ite.next();

    		if (aula.getMaterias().isEmpty())
    			ite.remove();
    	}
	}
}
