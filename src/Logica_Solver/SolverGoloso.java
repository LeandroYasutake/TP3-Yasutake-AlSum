package Logica_Solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;

public class SolverGoloso 
{
	private Instancia instancia;
	private ArrayList<Materia> materiasAux;
	private Subconjunto solucion;

	public SolverGoloso(Instancia inst)
	{
		this.instancia = inst;
		this.solucion = new Subconjunto();
		materiasAux = instancia.getListadoMaterias().clonarMaterias();
	}
	
	public void resolverSimple()
	{
		Collections.sort(materiasAux, (m1, m2) -> m1.getHoraFin() - m2.getHoraFin());
		asignarAula();
	}

	private void asignarAula() 
	{
		ListadoMaterias aula = new ListadoMaterias();
		
		aula.addMateria(materiasAux.get(0));
		materiasAux.remove(0);
		
		Iterator<Materia> ite = materiasAux.iterator();
    	
    	while(ite.hasNext()) 
    	{
    		Materia ultimaMatAgregada = aula.getMateria(aula.getNumeroMaterias()-1);
    		Materia iTmateria = ite.next();

    		if (iTmateria.getHoraInicio() >= ultimaMatAgregada.getHoraFin())
    		{
    			aula.addMateria(iTmateria);
    			ite.remove();
    		}
    	}
    	
    	this.solucion.getAulas().add(aula);
		
		if (!materiasAux.isEmpty())
			resolverSimple();
	}
	
	public ArrayList<ListadoMaterias> getAulasAsignadas()
	{
		return solucion.getAulas();
	}
}
