package Logica_Solver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import Logica_Grafo.Grafo;
import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;

public class SolverColoreoGoloso 
{
	private Instancia instancia;
	private Subconjunto solucion;

	public SolverColoreoGoloso(Instancia inst)
	{
		this.instancia = inst;
		this.solucion = new Subconjunto();
	}
	
	public void resolverConColor()
	{
		Grafo grafoMaterias = instancia.getListadoMaterias().crearGrafo();
		int[] coloresAsignados = getGrafoColor(grafoMaterias);
		asignarAulasColoreadas(coloresAsignados);
	}
	
	public static int[] getGrafoColor(Grafo grafo)
	{
		int[] resultado = new int[grafo.cantidadDeVertices()];
		Arrays.fill(resultado, -1);

		for (int vertice = 0; vertice < grafo.cantidadDeVertices() ; vertice++)
		{
			HashSet<Integer> coloresAsignados = new HashSet<Integer>();

			for (int vecino : grafo.vecinos(vertice))
				if (resultado[vecino] != -1)
					coloresAsignados.add(resultado[vecino]);

			resultado[vertice] = getPrimerColorLibre(coloresAsignados);
		}
		return resultado;
	}
	
	private static int getPrimerColorLibre(HashSet<Integer> coloresAsignados)
	{
		int ret = 0;
		for (int c : coloresAsignados)
		{
			if (ret != c)
				return ret;;
			ret++;
		}
		return ret;
	}
	
	private void asignarAulasColoreadas(int[] coloresAsignados)
	{
		HashSet<Integer> aulasSinRepetir = new HashSet<Integer>();
		
		getColoresUnicos(coloresAsignados, aulasSinRepetir);
		getAulasSinRepetir(aulasSinRepetir, solucion);
			
		for (int materia = 0 ; materia < coloresAsignados.length ; materia++)
		{
			ListadoMaterias aulaDestino = solucion.getAulas().get(coloresAsignados[materia]);
			Materia materiaAsignable = instancia.getMaterias().get(materia);
			
			aulaDestino.addMateria(materiaAsignable);
		}
	}

	public static void getColoresUnicos(int[] coloresAsignados, HashSet<Integer> aulasSinRepetir)
	{
		for (int color : coloresAsignados)
			aulasSinRepetir.add(color);
	}
	
	public static void getAulasSinRepetir(HashSet<Integer> aulasSinRepetir, Subconjunto solucion) 
	{
		for (int aulaUnica = 0 ; aulaUnica < aulasSinRepetir.size() ; aulaUnica++)
		{
			ListadoMaterias aula = new ListadoMaterias();
			solucion.getAulas().add(aula);
		}
	}
	
	public ArrayList<ListadoMaterias> getAulasAsignadas()
	{
		return solucion.getAulas();
	}
}
