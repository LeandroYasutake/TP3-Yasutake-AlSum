package Logica_Solver;

import java.util.ArrayList;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;

public class Instancia 
{
	private ListadoMaterias materias;

	public Instancia()
	{
		this.materias = new ListadoMaterias();
	}
	
	public ListadoMaterias getListadoMaterias()
	{
		return this.materias;
	}
	
	public ArrayList<Materia> getMaterias()
	{
		return this.materias.getMaterias();
	}
	
	public void setMaterias(ListadoMaterias materiasSinAulas) 
	{
		this.materias = materiasSinAulas;
	}
}
