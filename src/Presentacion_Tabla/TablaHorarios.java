package Presentacion_Tabla;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import Logica_Materias.ListadoMaterias;

public class TablaHorarios 
{
	private JTable tablaAulas;
	private ModeloHorarios modeloPropio;
	
	public TablaHorarios()
	{
		tablaAulas = new JTable();
		inicializarValores();
	}

	public void inicializarValores() 
	{
		tablaAulas.setCellSelectionEnabled(true);
		tablaAulas.setRowHeight(34);
		tablaAulas.setOpaque(true);
		tablaAulas.setFillsViewportHeight(true);
		tablaAulas.setBackground(new Color(255, 255, 228));
		tablaAulas.getTableHeader().setBackground(new Color(255, 255, 180));
		tablaAulas.getTableHeader().setReorderingAllowed(false);
	}
	
	public void updateModel(ArrayList<ListadoMaterias> aulasPadre)
	{
		modeloPropio = new ModeloHorarios(aulasPadre);
		tablaAulas.setModel(modeloPropio.getModelo());
		centrarTabla();
	}
	
	private void centrarTabla() 
	{
		 DefaultTableCellRenderer dtcr2 = new DefaultTableCellRenderer();
		 dtcr2.setHorizontalAlignment(JLabel.CENTER);
		 
		 controlarColumnas();
		 
		 for (int columna = 0 ; columna < tablaAulas.getColumnCount() ; columna++)
		 {
			 TableColumn col0 = tablaAulas.getColumnModel().getColumn(columna);
			
			 if (tablaAulas.getRowHeight() == 33)
				 tablaAulas.getColumnModel().getColumn(columna).setPreferredWidth(185);
			 
			 col0.setCellRenderer(dtcr2);
		 }
	}
	
	private void controlarColumnas() 
	{
		if (tablaAulas.getColumnCount() > 8)
		 {
			 tablaAulas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			 tablaAulas.setRowHeight(33);
		 }
			 
		 else
			 tablaAulas.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	}
	
	public JTable getTable()
	{
		return this.tablaAulas;
	}
	
	public ModeloHorarios getModel()
	{
		return this.modeloPropio;
	}
}
