package Presentacion_Tabla;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import Logica_Materias.ListadoMaterias;
import Logica_Materias.Materia;


public class ModeloHorarios 
{
	private ArrayList<ListadoMaterias> aulas;
	private DefaultTableModel model;
	
	public ModeloHorarios(ArrayList<ListadoMaterias> aulasPadre)
	{
		this.aulas = aulasPadre;
		inicializarModelo();
	}
	
	private void inicializarModelo() 
	{
		int cantidadFilas = 14;
		this.model = new DefaultTableModel(getNombresColumnas(), cantidadFilas)
		{
			private static final long serialVersionUID = 1L;

			@Override
		    public boolean isCellEditable(int row, int column) 
		    {
		       return false;
		    }
		};
		setearHorarios(cantidadFilas);
		cargarMaterias();
	}

	private String[] getNombresColumnas() 
	{
		String[] nombresCol = new String[aulas.size()+1];
		
		nombresCol[0] = "Horarios";
		
		for (int aula = 1 ; aula < this.aulas.size()+1 ; aula++)
			nombresCol[aula] = "Aula "+aula;
		
		return nombresCol;
	}

	private void setearHorarios(int cantidadFilas) 
	{
		String[] horarios = {"08:00 hs", "09:00 hs", "10:00 hs", "11:00 hs", "12:00 hs", "13:00 hs", "14:00 hs"
							, "15:00 hs", "16:00 hs", "17:00 hs", "18:00 hs", "19:00 hs", "20:00 hs", "21:00 hs"};
		
		for (int fila = 0 ; fila < cantidadFilas ; fila++)
			this.model.setValueAt(horarios[fila], fila, 0);
	}

	private void cargarMaterias() 
	{
		for (int aula = 0 ; aula < this.aulas.size() ; aula++)
			for (Materia m : this.aulas.get(aula).getMaterias())
			{
				int horarioFilaInicial = m.getHoraInicio()-8;
				
				while(horarioFilaInicial != m.getHoraFin()-8)
				{
					model.setValueAt(m.getNombre(), horarioFilaInicial, aula+1);
					horarioFilaInicial++;
				}
			}
	}

	public DefaultTableModel getModelo()
	{
		return this.model;
	}
}
